package br.ufsc.lapesd.riefederator.experiments;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Sets;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.Arrays.asList;

public class Measurements {
    private static Logger logger = LoggerFactory.getLogger(Measurements.class);

    public static final CSVFormat CSV_FMT = CSVFormat.EXCEL.withFirstRecordAsHeader().withTrim();
    public enum Headers {
        ExperimentName, ExperimentRun, Var, Value
    }


    private Map<String, String> var2value = new HashMap<>();
    private final String experimentName;
    private final int run;
    private final File measurementsFile;

    public Measurements(@Nonnull File destination, @Nonnull String experimentName, int run) {
        this.measurementsFile = destination;
        this.experimentName = experimentName;
        this.run = run;
        checkHeadersIfExists(measurementsFile);
    }
    public Measurements(@Nonnull File destination, @Nonnull String experimentName) {
        this.measurementsFile = destination;
        this.experimentName = experimentName;
        checkHeadersIfExists(destination);
        if (!destination.exists()) {
            run = 0;
        } else {
            int max = -1;
            try (CSVParser parser = CSV_FMT.parse(new FileReader(destination))) {
                for (CSVRecord record : parser) {
                    String value = record.get(Headers.ExperimentRun);
                    try {
                        int x = Integer.parseInt(value);
                        if (x > max) max = x;
                    } catch (NumberFormatException e) {
                        logger.warn("File {} has a badly formatted ExperimentRun: {}",
                                    destination, value);
                    }
                }

            } catch (IOException e) {
                logger.error("Failed to read file {}. Assuming this is run 0. " +
                             "Writing to the file will likely fail as well.", destination);
            }
            run = max+1;
            logger.info("Measurements({}, {}) guessed its run number as {}",
                        destination.getAbsolutePath(), experimentName, run);
        }
    }

    private void checkHeadersIfExists(@Nonnull File file) {
        if (!file.exists()) return;
        StringBuilder msgBuilder = new StringBuilder();
        try (CSVParser reader = CSV_FMT.parse(new FileReader(file))) {
            List<String> actual = reader.getHeaderNames();
            Set<String> expected = Arrays.stream(Headers.values())
                                         .map(Enum::name).collect(Collectors.toSet());
            Set<String> extra = Sets.difference(newHashSet(actual), expected);
            Set<String> missing = Sets.difference(expected, newHashSet(actual));
            boolean valid = extra.isEmpty() && missing.isEmpty();
            if (!valid) {
                msgBuilder.append("File ").append(file.getAbsolutePath())
                        .append(" exists. However, it ");
            }
            if (!extra.isEmpty()) {
                msgBuilder.append("has extra headers ").append(extra)
                          .append(missing.isEmpty() ? "." : " and ");
            }
            if (!missing.isEmpty())
                msgBuilder.append("misses headers ").append(missing);
            if (!valid)
                throw new IllegalArgumentException(msgBuilder.toString());
        } catch (FileNotFoundException ignored) {
        } catch (IOException e) {
            msgBuilder.append("File ").append(file).append(" exists, but could not be read");
            throw new IllegalArgumentException(msgBuilder.toString(), e);
        }
    }

    @CanIgnoreReturnValue
    public @Nonnull  Measurements save(@Nonnull String var, @Nullable Object value) {
        if (value == null) value = "";
        String old = var2value.put(var, value.toString());
        if (old != null)
            logger.warn("Value {} for measured var {} overwritten with {}", old, var, value);
        return this;
    }

    @CanIgnoreReturnValue
    public @Nonnull Measurements saveAndRestart(@Nonnull String var, @Nonnull Stopwatch sw) {
        save(var, sw.elapsed(TimeUnit.MICROSECONDS)/1000.0);
        sw.reset().start();
        return this;
    }

    public void write() throws IOException {
        checkHeadersIfExists(measurementsFile);
        boolean writeHeaders = !measurementsFile.exists();
        File parentFile = measurementsFile.getParentFile();
        if (!parentFile.exists()) {
            if (!parentFile.mkdirs()) {
                throw new IOException("Parent dir of "+measurementsFile.getAbsolutePath()+
                                      " does not exist, but could not be mkdir'ed");
            }
        }
        try (FileWriter writer = new FileWriter(measurementsFile, true);
             CSVPrinter printer = new CSVPrinter(writer, CSV_FMT)) {
            if (writeHeaders)
                printer.printRecord(asList(Headers.values()));
            for (Map.Entry<String, String> e : var2value.entrySet())
                printer.printRecord(asList(experimentName, run, e.getKey(), e.getValue()));
        }
    }
}
