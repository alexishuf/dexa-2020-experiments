package br.ufsc.lapesd.riefederator.sources;

import br.ufsc.lapesd.riefederator.description.SelectDescription;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.jena.query.ARQEndpoint;
import br.ufsc.lapesd.riefederator.query.Cardinality;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import br.ufsc.lapesd.riefederator.webapis.requests.paging.impl.ParamPagingStrategy;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.TermSerializer;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.impl.OnlyNumbersTermSerializer;
import br.ufsc.lapesd.riefederator.webapis.requests.parsers.impl.SimpleDateSerializer;
import br.ufsc.lapesd.riefederator.webapis.requests.rate.RateLimit;
import br.ufsc.lapesd.riefederator.webapis.requests.rate.RateLimitsRegistry;
import br.ufsc.lapesd.riefederator.webapis.requests.rate.impl.SimpleRateLimit;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BudgetEndpoints {
    private static final String RESOURCE_DIR = "portal_da_transparencia/";
    private static final String BASE = "http://www.transparencia.gov.br/api-de-dados/";
    public static final RateLimitsRegistry rateLimitsRegistry;

    private static final SimpleDateSerializer dateSerializer
            = new SimpleDateSerializer("dd/MM/yyy");

    static {
        rateLimitsRegistry = new RateLimitsRegistry();
        // official limits are 90/minute at 06:00-23:59 and 300/minute at 00:00-05:59
        // 60 requests (66%) should avoid being banned or reported.
        // Some of the APIs (at www.portaldatransparencia.gov.br) are not official and are used
        // by the public frontend. To be conservative, the same rate limit state
        // is applied to both (i.e., max 60 req/min whatever the host)
        RateLimit limit = SimpleRateLimit.perMinute(60);
        rateLimitsRegistry.register("www.transparencia.gov.br", limit);
        rateLimitsRegistry.register("www.portaldatransparencia.gov.br", limit);
    }

    public static @Nonnull WebAPICQEndpoint organizationsByCode() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"orgaos-siafi.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"orgaos-siafi{?codigo,pagina}")
                .atom2Input("Codigo", "codigo")
                .name("OrgaoPorCodigo")
                .pagingStrategy(ParamPagingStrategy.build("pagina"))
                .cardinality(Cardinality.upperBound(1))
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint organizationsByDescription() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"orgaos-siafi.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"orgaos-siafi{?descricao,pagina}")
                .atom2Input("Descricao", "descricao")
                .name("OrgaoPorDescricao")
                .pagingStrategy(ParamPagingStrategy.build("pagina"))
                .cardinality(Cardinality.upperBound(1))
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint contracts() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"contratos.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"contratos{?dataInicial,dataFinal,codigoOrgao,pagina}")
                .atom2Input("DataInicioVigencia", "dataInicial")
                .atom2Input("DataFimVigencia", "dataFinal")
                .atom2Input("CodigoSIAFIOrgaoVinculadoUnidadeGestora", "codigoOrgao")
                .name("Contratos")
                .input2Serializer("dataInicial", dateSerializer)
                .input2Serializer("dataFinal", dateSerializer)
                .pagingStrategy(ParamPagingStrategy.build("pagina"))
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint contractById() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"contratos-id.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"contratos/id{?id}")
                .atom2Input("Id", "id")
                .name("ContratoPorId")
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint procurements() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"licitacoes.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"licitacoes{?dataInicial,dataFinal,codigoOrgao,pagina}")
                .atom2Input("DataAberturaMinima", "dataInicial")
                .atom2Input("DataAberturaMaxima", "dataFinal")
                .atom2Input("CodigoSIAFIOrgaoVinculadoUnidadeGestora", "codigoOrgao")
                .name("LicitacaoDesc")
                .input2Serializer("dataInicial", dateSerializer)
                .input2Serializer("dataFinal", dateSerializer)
                .pagingStrategy(ParamPagingStrategy.build("pagina"))
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint procurementById() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"licitacoes-id.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"licitacoes/{id}")
                .atom2Input("Id", "id")
                .name("LicitacaoById")
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint procurementByUnitModalityAndNumber() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"licitacoes-por-uasg-modalidade-numero.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate(BASE+"licitacoes/por-uasg-modalidade-numero" +
                        "{?codigoUASG,numero,codigoModalidade}")
                .atom2Input("CodigoUnidadeGestora", "codigoUASG")
                .atom2Input("NumeroLicitacao", "numero")
                .atom2Input("CodigoModalidadeLicitacao", "codigoModalidade")
                .name("LicitacaoByUnitModalityAndNumber")
                .input2Serializer("numero", OnlyNumbersTermSerializer.builder()
                        .setFill('0').setWidth(9).build())
                .toEndpoint();
    }

    /** Undocumented Web API! */
    public static @Nonnull WebAPICQEndpoint contractorByFreetext() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"contratos-fornecedor-autocomplete.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate("http://www.portaldatransparencia.gov.br/criterios/" +
                        "contratos/fornecedor/autocomplete{?q}")
                .atom2Input("Name", "q")
                .name("FornecedorPorNome")
                .toEndpoint();
    }

    /** Undocumented Web API! */
    public static @Nonnull WebAPICQEndpoint contractsByContractorId() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"contratos-fornecedor.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate("http://www.portaldatransparencia.gov.br/contratos/" +
                        "consulta/resultado?{?fornecedor,offset}" +
                        "&paginacaoSimples=true&tamanhoPagina=15" +
                        "&direcaoOrdenacao=desc&colunaOrdenacao=dataFimVigencia" +
                        "&colunasSelecionadas=linkDetalhamento%2CdataAssinatura" +
                            "%2CdataPublicacaoDOU%2CdataInicioVigencia%2CdataFimVigencia" +
                            "%2CorgaoSuperior%2CorgaoEntidadeVinculada%2CunidadeGestora" +
                            "%2CformaContratacao%2CgrupoObjetoContratacao%2CnumeroContrato" +
                            "%2CnomeFornecedor%2CcpfCnpjFornecedor%2Csituacao%2CvalorContratado")
                .atom2Input("IdFornecedorData", "fornecedor")
                .name("ContratoPorIdFornecedor")
                .pagingStrategy(ParamPagingStrategy.builder("offset").withFirstPage(0)
                                                   .withEndOnJsonValue("/recordsTotal", 0)
                                                   .withIncrement(15).build())
                .toEndpoint();
    }

    private static @Nonnull WebAPICQEndpoint
    paymentsReceivedByCpfCnpj(@Nonnull String name, @Nonnull String atom,
                              @Nonnull TermSerializer serializer) {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"despesas-favorecido.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate("http://www.portaldatransparencia.gov.br/despesas/" +
                        "favorecido/resultado{?cpfCnpj,offset}" +
                        "&paginacaoSimples=true&tamanhoPagina=15&faseDespesa=3" +
                        "&direcaoOrdenacao=desc&colunaOrdenacao=valor" +
                        "&colunasSelecionadas=data%2CdocumentoResumido%2ClocalizadorGasto" +
                            "%2Cfase%2Cespecie%2Cfavorecido%2CufFavorecido%2Cvalor%2Cug%2Cuo" +
                            "%2Corgao%2CorgaoSuperior%2Cgrupo%2Celemento%2Cmodalidade")
                .atom2Input(atom, "cpfCnpj")
                .name(name)
                .input2Serializer("cpfCnpj", serializer)
                .pagingStrategy(ParamPagingStrategy.builder("offset").withFirstPage(0)
                                                   .withEndOnJsonValue("/recordsTotal", 0)
                                                   .withIncrement(15).build())
                .toEndpoint();
    }

    /** Undocumented Web API */
    public static @Nonnull WebAPICQEndpoint paymentsReceivedByCpf() {
        // xxx.yyy.zzz-ww
        // xxxyyyzzzww
        return paymentsReceivedByCpfCnpj("PagamentoPorCpf", "CpfData",
                OnlyNumbersTermSerializer.builder().setWidth(11).setFill('0').build());
    }
    /** Undocumented Web API */
    public static @Nonnull WebAPICQEndpoint paymentsReceivedByCnpj() {
        // xx.yyy.zzz/wwww-uu
        // xxyyyzzzwwwwuu
        return paymentsReceivedByCpfCnpj("PagamentoPorCnpj", "CnpjData",
                OnlyNumbersTermSerializer.builder().setWidth(14).setFill('0').build());
    }

    /** Undocumented Web API */
    public static @Nonnull WebAPICQEndpoint procurementsWithContractor() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"licitacoes-fornecedor.json")
                .rateLimitsRegistry(rateLimitsRegistry)
                .uriTemplate("http://www.portaldatransparencia.gov.br/pessoa-juridica/{id}" +
                        "/participante-licitacao/resultado{?id,offset}" +
                        "&paginacaoSimples=true&tamanhoPagina=15&direcaoOrdenacao=desc" +
                        "&colunaOrdenacao=numeroLicitacao&colunasSelecionadas=linkDetalhamento" +
                            "%2Corgao%2CunidadeGestora%2CnumeroLicitacao%2CdataAbertura")
                .atom2Input("IdFornecedorData", "id")
                .name("LicitacoesParticipante")
                .pagingStrategy(ParamPagingStrategy.builder("offset").withFirstPage(0)
                        .withEndOnJsonValue("/recordsTotal", 0)
                        .withIncrement(15).build())
                .toEndpoint();
    }

    public static @Nonnull ARQEndpoint procurementModalities() {
        Model model = ModelFactory.createDefaultModel();
        try (InputStream in = BudgetEndpoints.class.getResourceAsStream("modalidades.ttl")) {
            RDFDataMgr.read(model, in, Lang.TTL);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open embedded resource", e);
        }
        return ARQEndpoint.forModel(model, "procurementModalities");
    }

    public static @Nonnull Source procurementModalitiesSource() {
        ARQEndpoint ep = procurementModalities();
        assert ep.getName() != null;
        return new Source(new SelectDescription(ep), ep, ep.getName());
    }

    public static @Nonnull List<Source> all() {
        return Stream.concat(
                Stream.of(
                        organizationsByCode(), organizationsByDescription(),
                        contracts(), contractById(),
                        procurements(), procurementById(), procurementsWithContractor(),
                        procurementByUnitModalityAndNumber(),
                        contractorByFreetext(), contractsByContractorId(),
                        paymentsReceivedByCnpj(), paymentsReceivedByCpf()
                ).map(WebAPICQEndpoint::asSource),
                Stream.of(
                        procurementModalitiesSource()
                )
        ).collect(Collectors.toList());
    }
}
