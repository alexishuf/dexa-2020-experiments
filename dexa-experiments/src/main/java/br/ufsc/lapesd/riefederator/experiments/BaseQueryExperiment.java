package br.ufsc.lapesd.riefederator.experiments;

import br.ufsc.lapesd.riefederator.BaseApp;
import org.apache.commons.csv.CSVFormat;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Arrays.asList;

public abstract class BaseQueryExperiment extends BaseApp {
    private static final Logger logger = LoggerFactory.getLogger(BaseQueryExperiment.class);
    protected static final CSVFormat CSV_FMT = CSVFormat.EXCEL.withFirstRecordAsHeader();
    protected static final List<String> MEASURE_HEADERS =
            asList("ExperimentName","ExperimentRun","Var","Value");

    @Option(name = "--measurements-csv", required =  true,
            usage = "CSV that will receive performance measurements. Will be appended to if exists")
    protected File measurementsFile;

    @Option(name = "--results-csv-basename", required = true,
            usage = "Path and basename of csv with **query results**. Will receive the " +
                    "\"-$run.csv\" suffix for each repetition")
    protected String resultsCsvBasename;

    @Option(name = "--repetitions", usage = "How many times to repeat the experiment " +
            "using the same Federation instance")
    protected int repetitions = 1;

    protected Measurements measurement = null;

    protected @Nonnull String name() {
        return getClass().getSimpleName();
    }

    protected void checkOptions() {
        mkdirsParent(measurementsFile.getAbsolutePath());
        checkArgument(!measurementsFile.exists() || measurementsFile.isFile(),
                measurementsFile+" exists but is not a file!");
        mkdirsParent(resultsCsvBasename);
    }

    protected void mkdirsParent(@Nonnull String path) {
        File parent = new File(path).getParentFile();
        checkArgument(parent.exists() || parent.mkdirs(),
                parent+" does not exists and could not be mkdir()ed");
    }
}
