package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.webapis.description.PureDescriptive;
import org.apache.jena.vocabulary.RDF;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;

import static br.ufsc.lapesd.riefederator.jena.JenaWrappers.fromJena;

public class ProcurementsOfContractor extends BaseFederatedQueryExperiment {
    protected List<Lit> getDates() {
        return Arrays.asList(date("2019-01-01"), date("2019-12-31"));
    }

    @Override
    protected @Nonnull CQuery createQuery() {
        List<Lit> dates = getDates();
        Lit name = lit("Universidade Federal de Santa Catarina");
        return CQuery.with(
                        /* Contracts of the university */
                /*  0 */ new Triple(x,  p("dataInicioVigencia"), dates.get(0)),
                /*  1 */ new Triple(x,  p("dataFimVigencia"),    dates.get(1)),
                /*  2 */ new Triple(x,  p("unidadeGestora"),     x1),
                /*  3 */ new Triple(x1, p("orgaoVinculado"),     x2),
                /*  4 */ new Triple(x2, p("codigoSIAFI"),        t), // organization code
                /*  5 */ new Triple(x,  p("id"),                 b), // contract id
                /*  6 */ new Triple(x,  p("fornecedor"),         x3),
                /*  7 */ new Triple(x3, p("codigoFormatado"),    c), // contractor's CNPJ
                        /* Code of the university */
                /*  8 */ new Triple(y, p("codigo"),    t),
                /*  9 */ new Triple(y, p("descricao"), name),
                        /* Contract by id (get modalidadeCompra) */
                /* 10 */ new Triple(z,  p("id"),               b),
                /* 11 */ new Triple(z,  p("modalidadeCompra"), z1),
                /* 12 */ new Triple(z1, p("descricao"),        lit("Inexigibilidade de Licitação")),
                         /* Contractor CNPJ and surrogate internal id */
                /* 13 */ new Triple(w, p("id"),   s),
                /* 14 */ new Triple(w, p("name"), c), // text search, not an exact match
                        /* Procurements with a contractor*/
                /* 15 */ new Triple(u, p("skLicitacao"),  a),  // procurement ID
                /* 16 */ new Triple(u, p("idFornecedor"), s),
                        /* Get procurement attributes */
                /* 17 */ new Triple(v,  p("id"),                  a),
                /* 18 */ new Triple(v,  p("licitacao"),           v1),
                /* 19 */ new Triple(v,  p("valor"),               r1), //procurement value
                /* 20 */ new Triple(v,  p("modalidadeLicitacao"), v2),
                /* 21 */ new Triple(v1, p("objeto"),              r2), //procurement description
                /* 22 */ new Triple(v1, p("contatoResponsavel"),  r3), //civil servant's name
                /* 23 */ new Triple(v2, p("descricao"),           d),
                        /* Get the modalities for which the procurement process
                           is directed to specific companies (not open to public participation) */
                /* 24 */ new Triple(m, Modalities.hasDescription, d),
                /* 25 */ new Triple(m, fromJena(RDF.type),        Modalities.ApenasPreco)
        )
                .annotate( 0, PureDescriptive.INSTANCE) // x dataInicioVigencia
                .annotate( 1, PureDescriptive.INSTANCE) // x dataFimVigencia
                .annotate(14, PureDescriptive.INSTANCE) // w name
                .annotate(16, PureDescriptive.INSTANCE) // u idFornecedor
                .build();
    }

    public static void main(String[] args) throws Exception {
        new ProcurementsOfContractor().run(args);
    }

}
