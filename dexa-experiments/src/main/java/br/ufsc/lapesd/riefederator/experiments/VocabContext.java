package br.ufsc.lapesd.riefederator.experiments;

import br.ufsc.lapesd.riefederator.jena.JenaWrappers;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.model.term.std.StdLit;
import br.ufsc.lapesd.riefederator.model.term.std.StdPlain;
import br.ufsc.lapesd.riefederator.model.term.std.StdURI;
import br.ufsc.lapesd.riefederator.model.term.std.StdVar;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.ResourceFactory;

import javax.annotation.Nonnull;

public abstract class VocabContext {
    protected static final StdVar x = new StdVar("x");
    protected static final StdVar x1 = new StdVar("x1");
    protected static final StdVar x2 = new StdVar("x2");
    protected static final StdVar x3 = new StdVar("x3");
    protected static final StdVar x4 = new StdVar("x4");
    protected static final StdVar x5 = new StdVar("x5");
    protected static final StdVar x6 = new StdVar("x6");
    protected static final StdVar x7 = new StdVar("x7");
    protected static final StdVar x8 = new StdVar("x8");
    protected static final StdVar x9 = new StdVar("x9");

    protected static final StdVar y = new StdVar("y");
    protected static final StdVar y1 = new StdVar("y1");
    protected static final StdVar y2 = new StdVar("y2");
    protected static final StdVar y3 = new StdVar("y3");
    protected static final StdVar y4 = new StdVar("y4");
    protected static final StdVar y5 = new StdVar("y5");
    protected static final StdVar y6 = new StdVar("y6");
    protected static final StdVar y7 = new StdVar("y7");
    protected static final StdVar y8 = new StdVar("y8");
    protected static final StdVar y9 = new StdVar("y9");

    protected static final StdVar z = new StdVar("z");
    protected static final StdVar z1 = new StdVar("z1");
    protected static final StdVar z2 = new StdVar("z2");
    protected static final StdVar z3 = new StdVar("z3");
    protected static final StdVar z4 = new StdVar("z4");
    protected static final StdVar z5 = new StdVar("z5");
    protected static final StdVar z6 = new StdVar("z6");
    protected static final StdVar z7 = new StdVar("z7");
    protected static final StdVar z8 = new StdVar("z8");
    protected static final StdVar z9 = new StdVar("z9");

    protected static final StdVar w = new StdVar("w");
    protected static final StdVar w1 = new StdVar("w1");
    protected static final StdVar w2 = new StdVar("w2");
    protected static final StdVar w3 = new StdVar("w3");
    protected static final StdVar w4 = new StdVar("w4");
    protected static final StdVar w5 = new StdVar("w5");
    protected static final StdVar w6 = new StdVar("w6");
    protected static final StdVar w7 = new StdVar("w7");
    protected static final StdVar w8 = new StdVar("w8");
    protected static final StdVar w9 = new StdVar("w9");

    protected static final StdVar u = new StdVar("u");
    protected static final StdVar u1 = new StdVar("u1");
    protected static final StdVar u2 = new StdVar("u2");
    protected static final StdVar u3 = new StdVar("u3");
    protected static final StdVar u4 = new StdVar("u4");
    protected static final StdVar u5 = new StdVar("u5");
    protected static final StdVar u6 = new StdVar("u6");
    protected static final StdVar u7 = new StdVar("u7");
    protected static final StdVar u8 = new StdVar("u8");
    protected static final StdVar u9 = new StdVar("u9");

    protected static final StdVar v = new StdVar("v");
    protected static final StdVar v1 = new StdVar("v1");
    protected static final StdVar v2 = new StdVar("v2");
    protected static final StdVar v3 = new StdVar("v3");
    protected static final StdVar v4 = new StdVar("v4");
    protected static final StdVar v5 = new StdVar("v5");
    protected static final StdVar v6 = new StdVar("v6");
    protected static final StdVar v7 = new StdVar("v7");
    protected static final StdVar v8 = new StdVar("v8");
    protected static final StdVar v9 = new StdVar("v9");

    protected static final StdVar r = new StdVar("r");
    protected static final StdVar r1 = new StdVar("r1");
    protected static final StdVar r2 = new StdVar("r2");
    protected static final StdVar r3 = new StdVar("r3");
    protected static final StdVar r4 = new StdVar("r4");
    protected static final StdVar r5 = new StdVar("r5");
    protected static final StdVar r6 = new StdVar("r6");
    protected static final StdVar r7 = new StdVar("r7");
    protected static final StdVar r8 = new StdVar("r8");
    protected static final StdVar r9 = new StdVar("r9");

    protected static final StdVar a = new StdVar("a");
    protected static final StdVar b = new StdVar("b");
    protected static final StdVar c = new StdVar("c");
    protected static final StdVar d = new StdVar("d");
    protected static final StdVar e = new StdVar("e");
    protected static final StdVar m = new StdVar("m");
    protected static final StdVar n = new StdVar("n");
    protected static final StdVar s = new StdVar("s");
    protected static final StdVar t = new StdVar("t");

    protected static class Modalities {
        public static final String PREFIX = "https://alexishuf.bitbucket.io/dexa-2020/modalidades.ttl#";

        public static final StdURI Modalidade = new StdURI(PREFIX+"Modalidade");
        public static final StdURI Direcionada = new StdURI(PREFIX+"Direcionada");
        public static final StdURI ApenasPreco = new StdURI(PREFIX+"ApenasPreco");
        public static final StdURI Tecnica = new StdURI(PREFIX+"Tecnica");

        public static final StdURI hasDescription = new StdURI(PREFIX+"hasDescription");
        public static final StdURI hasCode = new StdURI(PREFIX+"hasCode");
    }

    protected static @Nonnull
    StdPlain p(@Nonnull String s) {
        return new StdPlain(s);
    }

    protected static @Nonnull
    Lit lit(@Nonnull String s) {
        return StdLit.fromUnescaped(s);
    }
    protected static @Nonnull Lit tlit(@Nonnull Object v) {
        return JenaWrappers.fromJena(ResourceFactory.createTypedLiteral(v));
    }
    protected static @Nonnull Lit date(@Nonnull String iso) {
        return JenaWrappers.fromJena(ResourceFactory.createTypedLiteral(iso, XSDDatatype.XSDdate));
    }
}
