package br.ufsc.lapesd.riefederator.sources;

import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import br.ufsc.lapesd.riefederator.webapis.requests.paging.impl.ParamPagingStrategy;
import com.google.common.base.Preconditions;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RepresentativesEndpoints {
    private static final String RESOURCE_DIR = "camara/";
    private static final String BASE = "https://dadosabertos.camara.leg.br/api/v2/";

    private static @Nonnull WebAPICQEndpoint representativesBy(@Nonnull String... atomOrInput) {
        Preconditions.checkArgument(atomOrInput.length > 0);
        Preconditions.checkArgument(atomOrInput.length % 2 == 0);

        List<String> inputs = new ArrayList<>();
        for (int i = 1; i < atomOrInput.length; i += 2)
            inputs.add(atomOrInput[i]);
        Example2Endpoint e2e = Example2Endpoint.fromResource(RESOURCE_DIR + "deputados.json")
                .uriTemplate(BASE + "deputados{?" + String.join(",", inputs) + ",pagina}");
        for (int i = 0; i < atomOrInput.length; i += 2)
            e2e.atom2Input(atomOrInput[i], atomOrInput[i + 1]);
        return e2e.name("ListaDeputados")
                .requiredByDefault()
                .pagingStrategy(ParamPagingStrategy.build("pagina"))
                .toEndpoint();
    }

    public static @Nonnull WebAPICQEndpoint representativesByState() {
        return representativesBy("SiglaUfDados", "siglaUf");
    }
    public static @Nonnull WebAPICQEndpoint representativesByParty() {
        return representativesBy("SiglaPartidoDados", "siglaPartido");
    }
    public static @Nonnull WebAPICQEndpoint representativesByStateAndParty() {
        return representativesBy("SiglaUfDados", "siglaUf",
                                 "SiglaPartidoDados", "siglaPartido");
    }

    public static @Nonnull WebAPICQEndpoint representativeById() {
        return Example2Endpoint.fromResource(RESOURCE_DIR+"deputados-id.json")
                .uriTemplate(BASE+"deputados/{id}")
                .atom2Input("IdDados", "id")
                .name("DadosDeputado")
                .toEndpoint();
    }

    public static @Nonnull List<Source> all() {
        return Stream.of(
                representativesByParty(), representativesByState(), representativeById()
        ).map(WebAPICQEndpoint::asSource).collect(Collectors.toList());
    }

}
