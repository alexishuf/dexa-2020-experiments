package br.ufsc.lapesd.riefederator.data;

import br.ufsc.lapesd.riefederator.BaseApp;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.hash.Hashing.sha256;
import static java.nio.charset.StandardCharsets.UTF_8;

public class RDFizeCompanyOwnership extends BaseApp {
    private static final CSVFormat IN_FMT = CSVFormat.EXCEL.withFirstRecordAsHeader();

    private static final String PREFIX = "https://alexishuf.bitbucket.io/dexa-2020/ownership.ttl#";
    private static final String type = RDF.type.getURI();
    private static final String xstring = XSD.xstring.getURI();
    private static final String xint = XSD.xint.getURI();
    private static final String Company = PREFIX+"Company";
    private static final String Ownership = PREFIX+"Ownership";
    private static final String owns = PREFIX+"owns";
    private static final String ownedBy = PREFIX+"ownedBy";
    private static final String ownership = PREFIX+"ownership";
    private static final String ownership_company = PREFIX+"ownership_company";
    private static final String ownership_agent = PREFIX+"ownership_agent";
    private static final String cpf = PREFIX+"cpf";
    private static final String cnpj = PREFIX+"cnpj";
    private static final String tax_id = PREFIX+"tax_id";
    private static final String ownership_begin = PREFIX+"ownership_begin";
    private static final String ownership_role_code = PREFIX+"ownership_role_code";
    private static final String ownership_role_name = PREFIX+"ownership_role_name";

    @Option(name = "--output", usage = "Output .nt.gz with RDFized data")
    private File outFile;

    @Option(name = "--prefix", usage = "URI prefix of minted URIs")
    private String uriPrefix = "https://alexishuf.bitbucket.io/dexa-2020/ownership-data/";

    @Argument(usage = "Uncensored CSV file with CNPJ information obtained with " +
                      "https://github.com/turicas/socios-brasil",
              required =  true, metaVar = "socios.csv.gz")
    private File inFile;

    public static void main(String[] args) throws Exception {
        new RDFizeCompanyOwnership().run(args);
    }

    @Override
    protected void run() throws Exception {
        checkArgument(!outFile.exists() || outFile.isFile(), outFile +" exits, but is not a file!");
        checkArgument(inFile.exists(), inFile +" does not exists!");
        checkArgument(inFile.canRead(), inFile +" has no read permissions!");

        Map<Integer, String> roleCode2Name = loadRoles();

        try (GZIPInputStream inputStream = new GZIPInputStream(new FileInputStream(inFile));
             CSVParser records = IN_FMT.parse(new InputStreamReader(inputStream));
             GZIPOutputStream outputStream = new GZIPOutputStream(new FileOutputStream(outFile));
             PrintStream out = new PrintStream(outputStream)) {
            for (CSVRecord row : records) {
                String companyId = row.get("cnpj"), ownerId = row.get("cnpj_cpf_do_socio");
                String companyUri = uriPrefix + companyId;

                String ownerUri;
                String ownershipUri;
                boolean hasCPF = isCPF(ownerId);
                boolean hasCNPJ = isCNPJ(ownerId);
                if (hasCPF || hasCNPJ) {
                    ownerUri = uriPrefix + ownerId;
                    ownershipUri = uriPrefix + "ownership/" + companyId + "/" + ownerId;
                } else {
                    String nome = row.get("nome_socio"), id = row.get("cnpj_cpf_do_socio");
                    String cat = (nome != null ? nome : "") + (id != null ? id : "");
                    //noinspection UnstableApiUsage
                    String hash = sha256().hashString(cat, UTF_8).toString();
                    ownerUri = uriPrefix + hash;
                    ownershipUri = uriPrefix + "ownership/" + companyId + "/" + hash;
                }

                /* company instance */
                out.printf("<%s> <%s> <%s> .\n", companyUri, type, Company);
                out.printf("<%s> <%s> <%s> .\n", companyUri, RDFizeCompanyOwnership.cnpj, companyId);
                out.printf("<%s> <%s> <%s> .\n", companyUri, ownership, ownershipUri);

                /* owner instance */
                if (hasCPF) {
                    out.printf("<%s> <%s> <%s> .\n", ownerUri, type, FOAF.Person.getURI());
                    out.printf("<%s> <%s> \"%s\"^^<%s> .\n", ownerUri, RDFizeCompanyOwnership.cpf, ownerId, xstring);
                } else if (hasCNPJ) {
                    out.printf("<%s> <%s> <%s> .\n", ownerUri, type, Company);
                    out.printf("<%s> <%s> \"%s\"^^<%s> .\n", ownerUri, RDFizeCompanyOwnership.cnpj, ownerId, xstring);
                } else  {
                    out.printf("<%s> <%s> <%s> .\n", ownerUri, type, FOAF.Agent.getURI());
                }
                out.printf("<%s> <%s> \"%s\"<%s> .\n",
                        ownerUri, FOAF.name.getURI(), row.get("nome_socio"), xstring);

                /* direct links between owner and company */
                out.printf("<%s> <%s> <%s> .\n", ownerUri,   owns,    companyUri);
                out.printf("<%s> <%s> <%s> .\n", companyUri, ownedBy, ownerUri  );

                /* ownership  relation */
                out.printf("<%s> <%s> <%s> .\n", ownershipUri, type, Ownership);
                out.printf("<%s> <%s> <%s> .\n", ownershipUri, ownership_company, companyUri);
                out.printf("<%s> <%s> <%s> .\n", ownershipUri, ownership_agent, ownerUri);
                String begin = row.get("data_entrada_sociedade");
                if (begin != null && begin.trim().matches("\\d\\d\\d\\d-\\d\\d-\\d\\d")) {
                    out.printf("<%s> <%s> \"%s\"^^<%s> .\n",
                               ownershipUri, ownership_begin, begin.trim(), xint);
                }
                String roleCode = row.get("codigo_qualificacao_socio");
                if (roleCode.matches("\\d+")) {
                    int code = Integer.parseInt(roleCode);
                    String name = roleCode2Name.get(code);
                    out.printf("<%s> <%s> %s^^<%s> .\n",
                               ownershipUri, ownership_role_code, code, xint);
                    if (name != null) {
                        out.printf("<%s> <%s> %s^^<%s> .\n",
                                   ownershipUri, ownership_role_name, name, xstring);
                    }
                }
            }
        }
    }

    private @Nonnull Map<Integer, String> loadRoles() throws IOException {
        Map<Integer, String> map = new HashMap<>();
        try (InputStream in = getClass().getResourceAsStream("qualificacao-socio.csv");
             CSVParser parse = IN_FMT.parse(new InputStreamReader(in))) {
            for (CSVRecord row : parse) {
                int code = Integer.parseInt(row.get("codigo"));
                String description = row.get("descricao");
                checkArgument(description != null,
                              "no field descricao in resource qualificacao-socio.csv!");
                map.put(code, description);
            }
        }
        return map;
    }

    private static boolean isCPF(@Nullable String cpf) {
        return cpf != null && cpf.length()==11 && cpf.matches("\\d+");
    }
    private static boolean isCNPJ(@Nullable String cnpj) {
        return cnpj != null && cnpj.length()==14 && cnpj.matches("\\d+");
    }
}
