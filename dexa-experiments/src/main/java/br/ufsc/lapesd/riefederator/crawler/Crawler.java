package br.ufsc.lapesd.riefederator.crawler;

import br.ufsc.lapesd.riefederator.BaseApp;
import com.google.common.base.Splitter;
import com.google.common.base.Stopwatch;
import com.google.gson.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.glassfish.jersey.uri.UriTemplate;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class Crawler  extends BaseApp {
    private static final Logger logger = LoggerFactory.getLogger(Crawler.class);
    private static final Pattern KV_RX = Pattern.compile("^([^=]*)=(.*)$");
    private static final Pattern ARRAY_PARAM_RX = Pattern.compile("^([^?]*)\\?(.*)$");
    private static final Pattern ISO_DATE_RX = Pattern.compile("^\\d\\d\\d\\d-\\d?\\d-\\d?\\d$");
    private static final Pattern BR_DATE_RX = Pattern.compile("^\\d?\\d/\\d?\\d/\\d\\d\\d\\d$");

    private static final CSVFormat TIMES_CSV_FMT = CSVFormat.EXCEL.withFirstRecordAsHeader();
    private static final List<String> TIMES_CSV_HEADERS = Arrays.asList("startDate", "endDate",
            "firstPage", "params", "outDir", "template", "uri", "datetime", "msecs", "status",
            "objects", "error");


    @Option(name = "--page-param", usage = "Param for paging in the template",
            forbids = "--no-paging")
    private String pagingParam = "pagina";

    @Option(name = "--no-paging", usage = "Disables paging with --page-param",
            forbids = "--page-param")
    private boolean noPaging = false;

    @Option(name = "--first-page", usage = "First page number. Default is 1.")
    private int firstPage = 1;

    @Option(name = "--page-increment",
            usage = "Step size for the value of --page-param parameter. Default is 1.")
    private int pageIncrement = 1;

    @Option(name = "--assume-last", usage = "Assume that a page is the last if it has " +
            "less than the given number of items in its array")
    private int assumeLast = 0;

    @Option(name = "--pretend", usage = "Do not send actual requests. " +
            "This will cause all paging attempts to stop at the first page")
    private boolean pretend = false;

    @Option(name = "--start-date-param")
    private @Nonnull String startDateParam = "dataInicial";

    @Option(name = "--end-date-param")
    private @Nonnull String endDateParam = "dataFinal";

    @Option(name = "--send-date-fmt", usage = "SimpleDateFormat of dates to be sent")
    private @Nonnull String sendDateFormat = "dd/MM/yyyy";

    @Option(name = "--start-date", usage = "Limit results to specified date. " +
            "Use ISO format (YYYY-MM-DD) or DD/MM/YYYY")
    private @Nullable String startDate;

    @Option(name = "--end-date", usage = "Limit results to specified date. " +
            "Use ISO format (YYYY-MM-DD) or DD/MM/YYYY")
    private @Nullable String endDate;

    @Option(name = "--no-month-limit", usage = "By default time intervals longer than a month " +
            "are transparently broken into month-long sub-intervals when building requests. " +
            "This disables that behavior")
    private boolean noMonthLimit = false;

    @Option(name = "--param", usage = "Give additional name=value parameter assignments")
    private String[] param = new String[0];

    @Option(name = "--array-param", usage = "Syntax: key=file.json?prop." +
            "Gets the values for the param \"key\" from file.json in the given property path",
            forbids = "--obj-param-path")
    private String arrayParam = null;

    @Option(name = "--anon-name", forbids = {"--id-property", "--naming-param"},
            usage = "Instead of using a --id-property to determine the filename of a object " +
                    "being saved, simply generate a new unique id")
    private boolean anonName = false;
    private @Nonnull AtomicInteger lastAnonId = new AtomicInteger(0);

    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    @Option(name = "--inject-property", usage = "Syntax: property=param. This will cause a " +
            "property named \"property\" to be injected at the root object during save() " +
            "with the value of the given parameter in the URI that originated that object " +
            "being saved.")
    private String[] injectProperty = new String[0];

    @Option(name = "--id-property")
    private @Nonnull String idProperty = "id";

    @Option(name = "--root-path", usage = "When parsing a JSON response, processing starts " +
            "from the root element. If the root element is a list, each member object will " +
            "be extracted to its own file. Some APIs return things like {\"data\" [...], " +
            "\"total\" 12345, ...}. In this case, setting --root-path to data will consider " +
            "that array as the root. Syntax: property/child/.../newRoot")
    private String rootPath = null;

    @Option(name = "--array-file", usage = "group all objects into a big array, including " +
                                           "objects without the --id-property")
    private String arrayFile = null;

    @Option(name = "--obj-param-dir", usage = "Apply --obj-param-path to " +
            "files that match --obj-param-dir-pattern in the given directory, " +
            "which must exist",
            depends = {"--obj-param-dir-pattern", "--obj-param-path"})
    private File objParamDir = null;

    @Option(name = "--obj-param-dir-pattern",
            usage = "Regular expression for files in --obj-param-dir",
            depends = {"--obj-param-dir", "--obj-param-path"})
    private String objParamDirPatternString = null;
    private Pattern objParamDirPattern = null;

    @Option(name = "--obj-param-path-filter",
            usage = "Syntax: some/path/expression=value. For every file that matches " +
                    "--obj-param-dir-pattern in --obj-param-dir, check if at least one solution for " +
                    "the path has the given value. If it does, --obj-param-path is honored, else " +
                    "the file is not considered for further requests generation.",
            depends = {"--obj-param-dir", "--obj-param-dir-pattern", "--obj-param-path"})
    private String objParamFilter = null;

    @Option(name = "--obj-param-path", usage = "Syntax: name=some/path/expression. Causes invocation " +
            "of URI_TEMPLATE binding param \"name\" to each value in the given path in every " +
            "object that matches --obj-param-dir-pattern in --obj-param-dir",
            depends = {"--obj-param-dir", "--obj-param-dir-pattern"})
    private String objParamPath = null;

    @Option(name = "--naming-param", depends = "--no-paging",
            usage = "Uses the given URI template param value to name the saved files")
    private String namingParam = null;

    @Option(name = "--delay-ms", usage = "Delay milliseconds between requests")
    private int delayMs = 0;

    @Option(name = "--out-dir", required = true, usage = "Where to save json files for each object")
    private File outDir;

    @Option(name = "--times-csv", usage = "Times CSV")
    private File timesCsv = null;

    @Option(name = "--accept-str", usage = "Contents of the Accept: header")
    private String acceptString = "application/json";

    @Argument(metaVar = "URI_TEMPLATE")
    private String uriTemplateString;
    private UriTemplate uriTemplate;

    public static void main(String[] args) throws Exception {
        new Crawler().run(args);
    }

    @Override
    public void run() throws Exception {
        checkArgument(!outDir.exists() || outDir.isDirectory(),
                "Output directory "+outDir+" exists but is not a directory");
        if (!outDir.exists() && !outDir.mkdirs())
            throw new IOException("Could not mkdir "+outDir);
        if (timesCsv == null)
            timesCsv = new File(outDir, "crawler-times.csv");
        uriTemplate = new UriTemplate(uriTemplateString);

        checkArgument(objParamDir == null || (objParamDir.exists() && objParamDir.isDirectory()),
                      "--obj-param-dir "+ objParamDir +" must exist and be a directory");
        if (objParamDirPatternString != null)
            objParamDirPattern = Pattern.compile(objParamDirPatternString);

        if (startDate != null && endDate != null) {
            checkArgument(parseDate(startDate).before(parseDate(endDate)),
                          "--start-date "+startDate+" should be before --end-date "+endDate);
        }

        if (startDate == null || endDate == null || noMonthLimit) {
            fetchAll();
        } else {
            GregorianCalendar start = new GregorianCalendar(), end = new GregorianCalendar();
            start.setTime(parseDate(startDate));
            end.setTime(parseDate(endDate));

            while (!start.equals(end)) {
                if (start.get(MONTH) == end.get(MONTH)
                        && start.get(YEAR) == end.get(YEAR)) {
                    fetchDate(start, end);
                    start = end;
                } else {
                    int maxDay = start.getActualMaximum(Calendar.DAY_OF_MONTH);
                    GregorianCalendar next = (GregorianCalendar)start.clone();
                    next.set(Calendar.DAY_OF_MONTH, maxDay);
                    fetchDate(start, next);
                    start = next;
                    start.add(Calendar.DAY_OF_MONTH, 1);
                    assert start.get(Calendar.DAY_OF_MONTH) == 1;
                }
            }
        }
    }

    private void fetchDate(@Nonnull Calendar start, @Nonnull Calendar end) throws IOException {
        String oldStart = this.startDate, oldEnd = this.endDate;
        try {
            SimpleDateFormat iso = new SimpleDateFormat("yyyy-MM-dd");
            this.startDate = iso.format(start.getTime());
            this.endDate = iso.format(end.getTime());
            fetchAll();
        } finally {
            this.startDate = oldStart;
            this.endDate = oldEnd;
        }
    }

    private List<JsonElement> getJsonPath(@Nonnull JsonElement root, @Nonnull String path) {
        List<String> segments = Splitter.on('/').trimResults().omitEmptyStrings()
                                                .splitToList(path);
        JsonElement e = root;
        for (int i = 0; i < segments.size(); i++) {
            if (e.isJsonArray()) {
                List<String> remainderList = segments.subList(i, segments.size());
                String remainder = String.join("/", remainderList);
                List<JsonElement> all = new ArrayList<>();
                for (JsonElement member : e.getAsJsonArray())
                    all.addAll(getJsonPath(member, remainder));
                return all;
            } else if (e.isJsonObject()) {
                JsonObject o = e.getAsJsonObject();
                if (!o.has(segments.get(i)))
                    return emptyList();
                e = o.get(segments.get(i));
            } else {
                return emptyList();
            }
        }
        return singletonList(e);
    }

    private void fetchAll() throws IOException {
        if (startDate != null && endDate != null) {
            logger.info("Fetching for period {} -- {} from page {} with template {}",
                    startDate, endDate, firstPage, uriTemplateString);
        } else {
            if (noPaging)
                logger.info("Fetching without paging from template {}", uriTemplateString);
            else
                logger.info("Fetching from page {} with template {}", firstPage, uriTemplateString);
            if (startDate != null)
                logger.info("--start-date {}, --end-date unset", startDate);
            if (endDate != null)
                logger.info("--start-date unset, --end-date {}", endDate);
        }

        for (Iterator<Map<String, String>> it = getOuterParamsIterator(); it.hasNext(); ) {
            Map<String, String> outer = it.next();
            if (noPaging) {
                Map<String, String> map = getAllParameters(0, outer);
                String uri = uriTemplate.createURI(map);
                String name = namingParam == null ? null : map.get(namingParam);
                if (name == null && anonName)
                    name = "anon-"+lastAnonId.incrementAndGet();
                doRequest(uri, name, map);
            } else {
                int objects = 0;
                int page = firstPage;
                while (true) {
                    Map<String, String> map = getAllParameters(page, outer);
                    String uri = uriTemplate.createURI(map);
                    int count = doRequest(uri, "page-" + page, map);
                    objects += count;
                    if (count == 0 || count < assumeLast)
                        break;
                    page += pageIncrement;
                }
                logger.info("Consumed {} pages and saved {} objects into {}",
                            page, objects, outDir);
            }
        }
    }

    private @Nonnull Iterator<Map<String, String>> getOuterParamsIterator() throws IOException {
        if (arrayParam == null) {
            if (objParamPath == null)
                return Collections.singleton(Collections.<String, String>emptyMap()).iterator();
            else
                return getObjParamsIterator();
        } else {
            return getArrayParamsIterator();
        }
    }

    private @Nonnull Iterator<Map<String, String>> getArrayParamsIterator() throws IOException {
        if (arrayParam == null)
            return Collections.singleton(Collections.<String, String>emptyMap()).iterator();
        Matcher matcher = KV_RX.matcher(arrayParam);
        checkArgument(matcher.matches(), "Bad syntax for --array-param: "+arrayParam);
        String key = matcher.group(1);
        String path = matcher.group(2);
        matcher = ARRAY_PARAM_RX.matcher(path);
        checkArgument(matcher.matches(), "Bad syntax for path in array: "+ path);
        String file = matcher.group(1);
        String property = matcher.group(2);
        List<String> values = new ArrayList<>();
        int notObject = 0, missingProperty = 0, cantString =0;
        try (FileReader reader = new FileReader(file)) {
            for (JsonElement e : new JsonParser().parse(reader).getAsJsonArray()) {
                if (!e.isJsonObject()) {
                    ++notObject;
                    continue;
                }
                JsonObject object = e.getAsJsonObject();
                if (!object.has(property)) {
                    ++missingProperty;
                    continue;
                }
                try {
                    values.add(object.get(property).getAsString());
                } catch (Exception ex) {
                    ++cantString;
                    logger.error("Could not get property {}", property, ex);
                }
            }
        }

        if (notObject > 0 || missingProperty > 0 || cantString > 0) {
            logger.warn("Problems when executing --array-param {}: {} non-objects in array, " +
                    "{} objects missing {} and {} objects whose property value could not be " +
                    "converted to string", arrayParam, notObject, missingProperty,
                                           property, cantString);
        }

        Iterator<String> it = values.iterator();
        return new Iterator<Map<String, String>>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public Map<String, String> next() {
                checkState(hasNext());
                Map<String, String> map = new HashMap<>();
                map.put(key, it.next());
                return map;
            }
        };
    }

    private @Nonnull Iterator<Map<String, String>> getObjParamsIterator() throws IOException {
        File[] files = objParamDir.listFiles((d, n) -> objParamDirPattern.matcher(n).matches());
        if (files == null)
            throw new IOException("Could not list files in "+objParamDir);
        List<String> pieces = Splitter.on('=').trimResults().omitEmptyStrings()
                                              .splitToList(objParamPath);
        checkArgument(pieces.size() == 2, "Bad syntax for --obj-param-path: "+objParamPath);
        String param = pieces.get(0), path = pieces.get(1);

        List<File> selectedFiles = selectFilesForObjParams(files);
        logger.info("Collecting values for --obj-param-path {} in {} files...",
                    objParamPath, selectedFiles.size());

        Set<String> values = new HashSet<>(selectedFiles.size());
        JsonParser jsonParser = new JsonParser();
        for (File file : selectedFiles) {
            try (FileReader reader = new FileReader(file)) {
                JsonElement root = jsonParser.parse(reader);
                for (JsonElement e : getJsonPath(root, path)) {
                    if (e.isJsonPrimitive()) {
                        values.add(e.getAsString());
                    } else if (e.isJsonArray()) {
                        for (JsonElement e2 : e.getAsJsonArray()) {
                            if (e2.isJsonPrimitive())
                                values.add(e2.getAsString());
                        }
                    }
                }
            } catch (IOException e) {
                logger.error("Ignoring exception while reading {}", file, e);
            }
        }

        logger.info("Found {} unique values for --obj-param-path {} in {} files.",
                    values.size(), objParamPath, selectedFiles.size());
        return values.stream().map(v -> {
            Map<String, String> map = new HashMap<>();
            map.put(param, v);
            return map;
        }).iterator();

    }

    private @Nonnull List<File> selectFilesForObjParams(@Nonnull File[] files) {
        Stopwatch sw = Stopwatch.createStarted();
        List<File> list = new ArrayList<>();
        JsonParser jParser = new JsonParser();
        if (objParamFilter == null) {
            logger.info("No --obj-param-path-filter. Selected {} " +
                        "files by --obj-param-dir-pattern={}", files.length, objParamDirPattern);
            return Arrays.asList(files);
        }

        List<String> pieces = Splitter.on('=').trimResults().omitEmptyStrings()
                                              .splitToList(objParamFilter);
        checkArgument(pieces.size() == 2,
                      "Invalid syntax for --obj-param-path-filter: "+objParamFilter);
        String path = pieces.get(0), expectedValue = pieces.get(1);

        logger.info("Filtering {} files in --obj-param-dir={} by --obj-param-dir-pattern={} " +
                    "and --obj-param-path-filter={}. This will take a while....",
                    files.length, objParamDir, objParamDirPattern, objParamFilter);
        Stopwatch statusSw = Stopwatch.createStarted();
        int count = 0;
        for (File file : files) {
            if (statusSw.elapsed(TimeUnit.SECONDS) > 10) {
                logger.info("Processed {}/{} files", count, files.length);
                statusSw.reset().start();
            }
            try (FileReader reader = new FileReader(file)) {
                JsonElement root = jParser.parse(reader);
                boolean valid = getJsonPath(root, path).stream().anyMatch(e -> {
                    if (e.isJsonArray()) {
                        for (JsonElement e2 : e.getAsJsonArray()) {
                            if (e2.isJsonPrimitive() && e2.getAsString().equals(expectedValue))
                                return true;
                        }
                    } else if (e.isJsonPrimitive()) {
                        return e.getAsString().equals(expectedValue);
                    }
                    return false;
                });
                if (valid)
                    list.add(file);
                ++count;
            } catch (IOException e) {
                logger.error("Ignoring error while reading {}", file, e);
            }
        }
        logger.info("Selected {} files out of {} in {} by {} in {} seconds",
                    list.size(), files.length, objParamDir, objParamFilter,
                    sw.elapsed(TimeUnit.MILLISECONDS)/1000.0);
        return list;
    }

    private void parseKVParams(@Nonnull Map<String, String> map) {
        for (String kv : param) {
            Matcher matcher = KV_RX.matcher(kv);
            checkArgument(matcher.matches(), "--param "+kv+" is invalid. Expected key=value");
            map.put(matcher.group(1), matcher.group(2));
        }
    }

    private @Nonnull String date2SendFormat(@Nonnull Date date) {
        return new SimpleDateFormat(sendDateFormat).format(date);
    }
    private @Nonnull Date parseDate(@Nonnull String string) {
        String trimmed = string.trim();
        try {
            if (ISO_DATE_RX.matcher(trimmed).matches()) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(trimmed);
            } else if (BR_DATE_RX.matcher(trimmed).matches()) {
                return new SimpleDateFormat("MM/dd/yyyy").parse(trimmed);
            } else {
                throw new IllegalArgumentException("Bad date syntax: "+string+
                                                   " use yyyy-MM-dd or dd/MM/yyyy");
            }
        } catch (ParseException e) {
            throw new RuntimeException("Unexpected exception", e);
        }
    }

    private @Nonnull Map<String, String>
    getAllParameters(int page, @Nonnull Map<String, String> outer) {
        Map<String, String> map = new HashMap<>(outer);
        parseKVParams(map);
        List<String> vars = uriTemplate.getTemplateVariables();
        if (startDate != null) {
            checkArgument(vars.contains(startDateParam),
                    "--start-date-param "+startDateParam+"not in template");
            map.put(startDateParam, date2SendFormat(parseDate(startDate)));
        }
        if (endDate != null) {
            checkArgument(vars.contains(endDateParam),
                    "--end-date-param "+endDateParam+"not in template");
            map.put(endDateParam, date2SendFormat(parseDate(endDate)));
        }
        if (!noPaging) {
            checkArgument(vars.contains(pagingParam),
                          "--page-param " + endDateParam + "not in template");
            map.put(pagingParam, String.valueOf(page));
        }
        return map;
    }

    private void checkTimesCSVHeaders() throws IOException {
        try (CSVParser records = TIMES_CSV_FMT.parse(new FileReader(timesCsv))) {
            if (!records.getHeaderNames().equals(TIMES_CSV_HEADERS)) {
                throw new IllegalStateException("File "+timesCsv+" has mismatching headers. " +
                        "Expected"+TIMES_CSV_HEADERS);
            }
        }
    }

    private int doRequest(@Nonnull String uri, @Nullable String name,
                          @Nonnull Map<String, String> params) throws IOException {
        if (pretend) {
            logger.info("Pretending doRequest({}). Zero objects", uri);
            return 0;
        }

        Date start = new Date();
        Stopwatch sw = Stopwatch.createStarted();
        Response response = ClientBuilder.newClient().target(uri).request(acceptString).get();
        int status = response.getStatus();
        double durationMs = sw.elapsed(TimeUnit.MICROSECONDS) / 1000.0;
        String error = null;
        int objects = 0, notObject = 0, noId = 0;

        if (status == 200) {
            try {
                JsonElement parsedRoot = new JsonParser().parse(response.readEntity(String.class));
                List<JsonElement> roots;
                if (rootPath != null)
                    roots = getJsonPath(parsedRoot, rootPath);
                else
                    roots = singletonList(parsedRoot);
                for (JsonElement root : roots) {
                    if (root.isJsonArray()) {
                        for (JsonElement e : root.getAsJsonArray()) {
                            if (!e.isJsonObject()) {
                                ++notObject;
                                continue;
                            }
                            JsonObject obj = e.getAsJsonObject();
                            if (!obj.has(idProperty) && !anonName) {
                                ++noId;
                                continue;
                            }
                            ++objects;

                            String basename = null;
                            if (anonName) {
                                basename = "anon-"+lastAnonId.incrementAndGet();
                            } else {
                                try {
                                    basename = obj.get(idProperty).getAsString();
                                } catch (Exception ex) {
                                    logger.error("Could not read id property {} as a string. " +
                                            "Will not save json file", idProperty, ex);
                                }
                            }
                            if (basename != null) {
                                injectProperties(obj, params);
                                save(basename + ".json", obj);
                            }
                        }
                        if (arrayFile != null) {
                            File file = new File(arrayFile);
                            JsonArray array = root.getAsJsonArray();
                            if (file.exists()) {
                                try (FileReader reader = new FileReader(file)) {
                                    JsonElement old = new JsonParser().parse(reader);
                                    JsonArray oldArray = old.getAsJsonArray();
                                    array = new JsonArray(array.size() + oldArray.size());
                                    oldArray.forEach(array::add);
                                    root.getAsJsonArray().forEach(array::add);
                                }
                            }
                            try (FileWriter writer = new FileWriter(file)) {
                                new GsonBuilder().setPrettyPrinting().create().toJson(array, writer);
                            }
                            objects = root.getAsJsonArray().size(); //everything was written
                        }
                    } else if (root.isJsonObject() && name != null) {
                        objects = 1;
                        String nameWithExt = name + (name.endsWith(".json") ? "" : ".json");
                        injectProperties(root, params);
                        save(nameWithExt, root.getAsJsonObject());
                    } else {
                        error = "Response JSON is not a array nor an object";
                        logger.warn("{}: {}", error, root);
                    }
                }
            } catch (JsonSyntaxException | JsonIOException e) {
                error = e.getClass().getSimpleName();
                logger.error("Problem parsing response JSON", e);
            }
            if (notObject > 0 || noId > 0) {
                if (objects == 0) {
                    error = "All values in array miss "+idProperty+" ("+noId+
                            ") or are not objects ("+notObject+")";
                    logger.warn(error);
                } else if (arrayFile == null) {
                    logger.info("{} non-objects in root array and {} objects in array without {}",
                            notObject, noId, idProperty);
                }
            }
        } else {
            error = "Non-200 response";
        }
        logRequest(start, uri, durationMs, status, objects, error);
        if (delayMs > 0) {
            try {
                Thread.sleep(delayMs);
            } catch (InterruptedException e) {
                logger.info("Interrupted from delay sleep.");
            }
        }
        return objects;
    }

    private void injectProperties(@Nonnull JsonElement element,
                                  @Nonnull Map<String, String> params) {
        if (!(element instanceof JsonObject)) return;
        JsonObject object = element.getAsJsonObject();
        for (String kv : injectProperty) {
            List<String> pieces = Splitter.on('=').trimResults().omitEmptyStrings().splitToList(kv);
            if (pieces.size() != 2) {
                logger.error("invalid property=param given to --inject-property has {} pieces: {}", pieces.size(), kv);
            } else {
                String property = pieces.get(0);
                String param = pieces.get(1);
                String value = params.getOrDefault(param, null);
                object.add(property, new JsonPrimitive(value));
            }
        }

    }

    private void save(@Nonnull String filename, @Nonnull JsonObject object) throws IOException {
        File file = new File(outDir, filename);
        if (file.exists()) {
            checkArgument(!file.isDirectory(), "Cannot save file to "+file+". Exists as directory");
            logger.info("Overwriting {}", file);
        }
        try (FileWriter writer = new FileWriter(file)) {
            new GsonBuilder().setPrettyPrinting().create().toJson(object, writer);
        }
    }

    private void logRequest(@Nonnull Date start, @Nonnull String uri,
                            double millisecondsDuration, int status, int objects,
                            @Nullable String error) throws IOException {
        logger.info("{}: {} in {}ms got {} objects", status, uri, millisecondsDuration, objects);
        boolean writeHeaders = !timesCsv.exists();
        if (!writeHeaders)
            checkTimesCSVHeaders();
        try (CSVPrinter p = new CSVPrinter(new FileWriter(timesCsv, true), TIMES_CSV_FMT)) {
            if (writeHeaders)
                p.printRecord(TIMES_CSV_HEADERS);
            String iso = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(start);
            p.printRecord(startDate, endDate, firstPage, String.join(";", param),
                          outDir, uriTemplateString, uri, iso, millisecondsDuration, status,
                          objects, error);
        }
    }
}
