package br.ufsc.lapesd.riefederator;

import br.ufsc.lapesd.riefederator.crawler.Crawler;
import br.ufsc.lapesd.riefederator.data.RDFizeCompanyOwnership;
import br.ufsc.lapesd.riefederator.data.RDFizeCrawl;
import br.ufsc.lapesd.riefederator.data.Table2Document;
import br.ufsc.lapesd.riefederator.experiments.federated.ProcurementForContract;
import br.ufsc.lapesd.riefederator.experiments.federated.ProcurementsOfContractor;
import br.ufsc.lapesd.riefederator.experiments.tdb.TDBProcurementForContract;
import br.ufsc.lapesd.riefederator.experiments.tdb.TDBProcurementsOfContractor;

import java.io.PrintStream;
import java.util.Arrays;

import static java.util.stream.Collectors.joining;

public class ExperimentsApp {
    public enum Command {
        CRAWL,
        RDFSOCIOS,
        TABLE2DOC,
        RDF_CRAWL,
        EXP_PROC4CONTRACT,
        EXP_PROC_OF_CONTRACTOR,
        EXP_TDB_PROC4CONTRACT,
        EXP_TDB_PROC_OF_CONTRACTOR;

        public void call(String[] args) throws Exception {
            if (this == CRAWL) {
                Crawler.main(args);
            } else if (this == TABLE2DOC) {
                Table2Document.main(args);
            } else if (this == RDFSOCIOS) {
                RDFizeCompanyOwnership.main(args);
            } else if (this == RDF_CRAWL) {
                RDFizeCrawl.main(args);
            } else if (this == EXP_PROC4CONTRACT) {
                ProcurementForContract.main(args);
            } else if (this == EXP_PROC_OF_CONTRACTOR) {
                ProcurementsOfContractor.main(args);
            } else if (this == EXP_TDB_PROC4CONTRACT) {
                TDBProcurementForContract.main(args);
            } else if (this == EXP_TDB_PROC_OF_CONTRACTOR) {
                TDBProcurementsOfContractor.main(args);
            } else {
                throw new UnsupportedOperationException();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            printHelp(System.err);
            return;
        }
        if (args[0].equals("--help") || args[0].equals("-h")) {
            printHelp(System.out);
            return;
        }

        Command command;
        try {
            command = Command.valueOf(args[0]);
        } catch (IllegalArgumentException e) {
            System.err.println("Bad command: "+args[0]);
            printHelp(System.err);
            return;
        }

        command.call(Arrays.copyOfRange(args, 1, args.length));
    }

    private static void printHelp(PrintStream stream) {
        stream.println("Usage: \n" +
                "    java -jar dexa-experiments-1.0-SNAPSHOT.jar  COMMAND|-h|--help [args...]\n" +
                "Commands:\n" +
                Arrays.stream(Command.values()).map(c -> "    "+c).collect(joining("\n")) +
                "\n" +
                "Options:\n" +
                "    -h|--help  Show this help message and exits");
    }

}
