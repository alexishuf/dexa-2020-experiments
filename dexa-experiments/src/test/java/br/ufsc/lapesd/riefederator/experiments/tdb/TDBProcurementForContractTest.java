package br.ufsc.lapesd.riefederator.experiments.tdb;

import org.apache.jena.query.*;
import org.apache.jena.system.Txn;
import org.apache.jena.tdb2.TDB2Factory;
import org.testng.SkipException;
import org.testng.annotations.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.assertEquals;

public class TDBProcurementForContractTest {

    @Test
    public void testExecute() {
        File tdbDir = new File("nobck/tdb");
        if (!tdbDir.exists())
            throw new SkipException("tdb dir at "+tdbDir+" does not exist");
        Dataset dataset = TDB2Factory.connectDataset(tdbDir.getAbsolutePath());

        String query = new TDBProcurementForContract().getQuery();
        List<QuerySolution> list = new ArrayList<>();
        Txn.executeRead(dataset, () -> {
            try (QueryExecution exec = QueryExecutionFactory.create(query, dataset)) {
                ResultSet resultSet = exec.execSelect();
                resultSet.forEachRemaining(list::add);
            }
        });
        assertEquals(list.size(), 4);
    }
}