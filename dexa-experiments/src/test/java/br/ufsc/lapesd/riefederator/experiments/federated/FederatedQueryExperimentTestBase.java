package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.TestUtils;
import br.ufsc.lapesd.riefederator.experiments.VocabContext;
import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.federation.decomp.DecompositionStrategy;
import br.ufsc.lapesd.riefederator.federation.tree.PlanNode;
import br.ufsc.lapesd.riefederator.federation.tree.QueryNode;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.query.TPEndpoint;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.*;

import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.setMinus;
import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.streamPreOrder;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public abstract class FederatedQueryExperimentTestBase extends VocabContext {
    protected final @Nonnull CQuery query = createExperiment().createQuery();

    protected abstract @Nonnull BaseFederatedQueryExperiment createExperiment();
    /** Identifiers that are relevant (before planning) */
    protected abstract @Nonnull List<String> relevantSourceIdentifiers();
    /** Identifiers that are relevant (before planning) and will yield a single QueryNode */
    protected abstract @Nonnull List<String> uniquelyRelevantSourceIdentifiers();
    /** Identifiers for sources that can be part of a plan */
    protected abstract @Nonnull List<String> allowedSourceIdentifiers();

    @Test(groups = {"local"})
    public void testUniqueSourceIdentifyingStrings() {
        Collection<Source> sources = createExperiment().getSources();
        assertEquals(sources.size(),
                     sources.stream().map(TestUtils::getIdentifyingString).distinct().count(),
                     "There are duplicate source names!");
    }

    @Test(groups = {"local"})
    public void testDecompose() {
        ProcurementForContract exp = new ProcurementForContract();
        Collection<Source> sources = exp.getSources();
        Set<TPEndpoint> expectedEps = relevantSourceIdentifiers().stream()
                .map(n -> TestUtils.getByIdentifyingString(sources, n).getEndpoint())
                .collect(toSet());

        Set<TPEndpoint> expectedUniqueEps = uniquelyRelevantSourceIdentifiers().stream()
                .map(n -> TestUtils.getByIdentifyingString(sources, n).getEndpoint())
                .collect(toSet());

        try (Federation federation = exp.createFederation(sources)) {
            DecompositionStrategy strategy = federation.getDecompositionStrategy();
            Collection<QueryNode> nodes = strategy.decomposeIntoLeaves(query);
            List<TPEndpoint> eps = nodes.stream().map(QueryNode::getEndpoint).collect(toList());

            StringBuilder b = new StringBuilder();
            b.append( "\nUnexpected endpoints: {\n");
            setMinus(eps, expectedEps).forEach(e -> b.append("  ").append(e).append('\n'));
            b.append("}\nMissing endpoints: {\n");
            setMinus(expectedEps, eps).forEach(e -> b.append("  ").append(e).append('\n'));
            b.append('}');
            //noinspection SimplifiedTestNGAssertion (better readability)
            assertTrue(new HashSet<>(eps).equals(expectedEps), b.toString());

            List<TPEndpoint> webEPs = eps.stream().filter(WebAPICQEndpoint.class::isInstance)
                    .filter(expectedUniqueEps::contains)
                    .collect(toList());
            assertEquals(webEPs.size(), new HashSet<>(webEPs).size(),
                    "There are WebAPICQEndpoints being selected more than once");
        }
    }

    @Test(groups = {"local"})
    public void testPlan() {
        BaseFederatedQueryExperiment exp = createExperiment();
        for (int i = 0; i < 32; i++) {
            ArrayList<Source> sourcesList = new ArrayList<>(exp.getSources());
            if (i > 0)
                Collections.shuffle(sourcesList);
            for (int j = 0; j < 32; j++) {
                CQuery reordered;
                if (i > 0) {
                    ArrayList<Triple> triplesList = new ArrayList<>(query.getSet());
                    Collections.shuffle(triplesList);
                    reordered = CQuery.with(triplesList).copyAnnotations(query).build();
                } else {
                    reordered = query;
                }
                try (Federation federation = exp.createFederation(sourcesList)) {
                    PlanNode plan = federation.plan(reordered);
                    TestUtils.assertPlanAnswers(plan, reordered);
                    checkPlan(plan);

                    Set<TPEndpoint> allowed = allowedSourceIdentifiers().stream()
                            .map(n -> TestUtils.getByIdentifyingString(sourcesList, n).getEndpoint())
                            .collect(toSet());
                    List<QueryNode> badQNodes = streamPreOrder(plan)
                            .filter(QueryNode.class::isInstance).map(n -> (QueryNode) n)
                            .filter(n -> !allowed.contains(n.getEndpoint()))
                            .collect(toList());
                    assertEquals(badQNodes, emptySet());
                }
            }
        }
    }

    protected abstract void checkPlan(@Nonnull PlanNode planNode);
}
