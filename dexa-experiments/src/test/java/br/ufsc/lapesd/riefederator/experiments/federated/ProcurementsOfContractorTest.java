package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.TestUtils;
import br.ufsc.lapesd.riefederator.description.CQueryMatch;
import br.ufsc.lapesd.riefederator.description.Description;
import br.ufsc.lapesd.riefederator.description.molecules.Molecule;
import br.ufsc.lapesd.riefederator.description.molecules.MoleculeMatcher;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.federation.tree.JoinNode;
import br.ufsc.lapesd.riefederator.federation.tree.PlanNode;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.reason.tbox.OWLAPITBoxReasoner;
import br.ufsc.lapesd.riefederator.sources.BudgetEndpoints;
import br.ufsc.lapesd.riefederator.webapis.description.APIMoleculeMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.apache.jena.vocabulary.RDF;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.setMinus;
import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.streamPreOrder;
import static br.ufsc.lapesd.riefederator.jena.JenaWrappers.fromJena;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.testng.Assert.*;

public class ProcurementsOfContractorTest extends FederatedQueryExperimentTestBase {

    private static final Lit name = lit("Universidade Federal de Santa Catarina");

    @Override
    protected @Nonnull
    BaseFederatedQueryExperiment createExperiment() {
        return new ProcurementsOfContractor();
    }

    @Override
    protected @Nonnull List<String> relevantSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "ContratoPorId",
                "FornecedorPorNome",
                "LicitacaoById",
                "LicitacoesParticipante",
                TestUtils.getIdentifyingString(BudgetEndpoints.procurementModalitiesSource()),
                /* These also match, but should not be used in plans */
                "OrgaoPorCodigo"
        );
    }

    @Override
    protected @Nonnull List<String> uniquelyRelevantSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "FornecedorPorNome",
                "LicitacaoById",
                "LicitacoesParticipante",
                TestUtils.getIdentifyingString(BudgetEndpoints.procurementModalitiesSource()),
                /* These also match, but should not be used in plans */
                "OrgaoPorCodigo"
        );
    }

    @Override
    protected @Nonnull List<String> allowedSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "ContratoPorId",
                "FornecedorPorNome",
                "LicitacaoById",
                "LicitacoesParticipante",
                TestUtils.getIdentifyingString(BudgetEndpoints.procurementModalitiesSource())
        );
    }

    @DataProvider
    public static Object[][] egMatchData() {
        return Stream.of(
                asList(BudgetEndpoints.contracts().asSource(), CQuery.from(
                        /*  0 */ new Triple(x,  p("dataInicioVigencia"), date("2019-01-01")),
                        /*  1 */ new Triple(x,  p("dataFimVigencia"),    date("2019-12-31")),
                        /*  2 */ new Triple(x,  p("unidadeGestora"),     x1),
                        /*  3 */ new Triple(x1, p("orgaoVinculado"),     x2),
                        /*  4 */ new Triple(x2, p("codigoSIAFI"),        lit("26246")), // organization code
                        /*  5 */ new Triple(x,  p("id"),                 b), // contract id
                        /*  6 */ new Triple(x,  p("fornecedor"),         x3),
                        /*  7 */ new Triple(x3, p("codigoFormatado"),    c) // contractor's CNPJ

                )),
                asList(BudgetEndpoints.organizationsByDescription().asSource(), CQuery.from(
                        /*  7 */ new Triple(y, p("codigo"), t),
                        /*  8 */ new Triple(y, p("descricao"), name)
                )),
                asList(BudgetEndpoints.contractById().asSource(), CQuery.from(
                        /* 10 */ new Triple(z,  p("id"),               lit("54136323")),
                        /* 11 */ new Triple(z,  p("modalidadeCompra"), z1),
                        /* 12 */ new Triple(z1, p("descricao"),        lit("Inexigibilidade de Licitação"))
                )),
                asList(BudgetEndpoints.contractorByFreetext().asSource(), CQuery.from(
                        /* 13 */ new Triple(w, p("id"), s),
                        /* 14 */ new Triple(w, p("name"), lit("18.628.083/0001-23"))
                )),
                asList(BudgetEndpoints.procurementsWithContractor().asSource(), CQuery.from(
                        /* 15 */ new Triple(u, p("skLicitacao"), a),  // procurement ID
                        /* 16 */ new Triple(u, p("idFornecedor"), lit("22138623"))
                )),
                asList(BudgetEndpoints.procurementById().asSource(), CQuery.from(
                        /* 17 */ new Triple(v, p("id"),                  lit("260155480")),
                        /* 18 */ new Triple(v, p("licitacao"),           v1),
                        /* 19 */ new Triple(v, p("valor"),               r1), //procurement value
                        /* 20 */ new Triple(v, p("modalidadeLicitacao"), v2),
                        /* 21 */ new Triple(v1, p("objeto"),             r2), //procurement description
                        /* 22 */ new Triple(v1, p("contatoResponsavel"), r3), //civil servant's name
                        /* 23 */ new Triple(v2, p("descricao"),          d)
                )),
                asList(BudgetEndpoints.procurementModalitiesSource(), CQuery.from(
                        /* 24 */ new Triple(m, Modalities.hasDescription, d),
                        /* 25 */ new Triple(m, fromJena(RDF.type), Modalities.ApenasPreco)
                ))
        ).map(List::toArray).toArray(Object[][]::new);
    }

    @Test(dataProvider = "egMatchData", groups = {"local"})
    public void testMatchEG(@Nonnull Source source, @Nonnull CQuery eg) {
        CQueryMatch match = source.getDescription().match(eg);
        assertFalse(match.isEmpty());
        if (source.getDescription() instanceof APIMoleculeMatcher) {
            assertEquals(match.getKnownExclusiveGroups().size(), 1);
            assertEquals(match.getKnownExclusiveGroups().get(0).getSet(), eg.getSet());
        } else {
            Set<Triple> missing = setMinus(eg, match.getAllRelevant());
            assertEquals(missing, emptySet(), "There are missing triples");
            assertTrue(eg.containsAll(match.getAllRelevant()), "Extraneous triples matched!");
        }
    }

    @DataProvider
    public static Object[][] matchData() {
        return Stream.of(
                asList(BudgetEndpoints.contracts().asSource(), CQuery.from(
                        /*  0 */ new Triple(x,  p("dataInicioVigencia"), date("2019-01-01")),
                        /*  1 */ new Triple(x,  p("dataFimVigencia"),    date("2019-12-31")),
                        /*  2 */ new Triple(x,  p("unidadeGestora"),     x1),
                        /*  3 */ new Triple(x1, p("orgaoVinculado"),     x2),
                        /*  4 */ new Triple(x2, p("codigoSIAFI"),        t), // organization code
                        /*  5 */ new Triple(x,  p("id"),                 b), // contract id
                        /*  6 */ new Triple(x,  p("fornecedor"),         x3),
                        /*  7 */ new Triple(x3, p("codigoFormatado"),    c) // contractor's CNPJ
                )),
                asList(BudgetEndpoints.organizationsByDescription().asSource(), CQuery.from(
                        /*  8 */ new Triple(y, p("codigo"),    t),
                        /*  9 */ new Triple(y, p("descricao"), name)
                )),
                asList(BudgetEndpoints.contractById().asSource(), CQuery.from(
                        /* 10 */ new Triple(z,  p("id"),               b),
                        /* 11 */ new Triple(z,  p("modalidadeCompra"), z1),
                        /* 12 */ new Triple(z1, p("descricao"),        lit("Inexigibilidade de Licitação"))
                )),
                asList(BudgetEndpoints.contractorByFreetext().asSource(), CQuery.from(
                        /* 13 */ new Triple(w, p("id"),   s),
                        /* 14 */ new Triple(w, p("name"), c) // text search, not an exact match
                )),
                asList(BudgetEndpoints.procurementsWithContractor().asSource(), CQuery.from(
                        /* 15 */ new Triple(u, p("skLicitacao"),  a),  // procurement ID
                        /* 16 */ new Triple(u, p("idFornecedor"), s)
                )),
                asList(BudgetEndpoints.procurementById().asSource(), CQuery.from(
                        /* 17 */ new Triple(v,  p("id"),                  a),
                        /* 18 */ new Triple(v,  p("licitacao"),           v1),
                        /* 19 */ new Triple(v,  p("valor"),               r1), //procurement value
                        /* 20 */ new Triple(v,  p("modalidadeLicitacao"), v2),
                        /* 21 */ new Triple(v1, p("objeto"),              r2), //procurement description
                        /* 22 */ new Triple(v1, p("contatoResponsavel"),  r3), //civil servant's name
                        /* 23 */ new Triple(v2, p("descricao"),           d)
                )),
                asList(BudgetEndpoints.procurementModalitiesSource(), CQuery.from(
                        /* 24 */ new Triple(m, Modalities.hasDescription, d),
                        /* 25 */ new Triple(m, fromJena(RDF.type),        Modalities.ApenasPreco)
                ))
        ).map(List::toArray).toArray(Object[][]::new);
    }

    @Test(dataProvider = "matchData", groups = {"local"})
    public void testNonAPIMatch(@Nonnull Source source, @Nonnull CQuery eg) {
        if (!(source.getDescription() instanceof MoleculeMatcher))
            return; //skip
        Description description = source.getDescription();
        if (description instanceof APIMoleculeMatcher) {
            Molecule molecule = ((APIMoleculeMatcher) source.getDescription()).getMolecule();
            description = new MoleculeMatcher(molecule, OWLAPITBoxReasoner.structural());
        }
        CQueryMatch match = description.match(query);
        assertFalse(match.isEmpty(), "No matches!");
        ImmutableList<CQuery> egs = match.getKnownExclusiveGroups();

        assertEquals(egs.stream().filter(CQuery::isEmpty).collect(toList()),
                emptySet(), "Empty exclusive groups!");

        List<ImmutableSet<Triple>> matches = egs.stream().map(CQuery::getSet)
                .filter(eg.getSet()::equals).collect(toList());
        assertEquals(matches.size(), 1, "Expected one occurrence of the EG");
    }

    @Test(dataProvider = "matchData", groups = {"local"})
    public void testMatch(@Nonnull Source source, @Nonnull CQuery eg) {
        CQueryMatch match = source.getDescription().match(query);
        if (!(source.getDescription() instanceof APIMoleculeMatcher)
                && !(source.getDescription() instanceof  MoleculeMatcher)) {
            assertFalse(match.isEmpty(),
                    "Expected a match of "+eg.size()+" triples for "+source.getEndpoint());
            assertTrue(match.getKnownExclusiveGroups().isEmpty(),
                    "Expected no EGs for non-molecule source");
            assertTrue(match.getNonExclusiveRelevant().containsAll(eg),
                    "Some triples that should match, did not");
            return; //end here
        }
        // check for EG matches
        if (match.isEmpty() && source.getDescription() instanceof APIMoleculeMatcher) {
            Molecule molecule = ((APIMoleculeMatcher) source.getDescription()).getMolecule();
            MoleculeMatcher m2 = new MoleculeMatcher(molecule, OWLAPITBoxReasoner.structural());
            CQueryMatch match2 = m2.match(query);
            assertFalse(match2.isEmpty(), "Not even plain molecule matches!");
        }

        Set<ImmutableSet<Triple>> matchedSets = match.getKnownExclusiveGroups().stream()
                .map(CQuery::getSet).collect(toSet());
        assertTrue(matchedSets.contains(eg.getSet()), "Expected EG not found");
    }

    @Override
    protected void checkPlan(@Nonnull PlanNode root) {
        List<PlanNode> joinsWithInputs = streamPreOrder(root).filter(JoinNode.class::isInstance)
                .filter(n -> !n.getInputVars().isEmpty()).collect(toList());
        assertEquals(joinsWithInputs, emptyList(), "This query should yield a plan " +
                "without input variables on join nodes");
    }

//    @Test(dependsOnGroups = {"local"})
//    public void testExecute() {
//        BaseQueryExperiment exp = createExperiment();
//        // procurements with open price competition
//        List<String> positiveIds = asList("265665020", "271577079");
//        // past procurements without price competition (directed procurement)
//        List<String> negativeIds = asList("266246100", "272578662");
//        try (Federation federation = exp.createFederation(exp.getSources())) {
//            List<String> ids = new ArrayList<>();
//            federation.query(query).forEachRemainingThenClose(
//                    s -> ids.add(Objects.requireNonNull(s.get(a)).asLiteral().getLexicalForm()));
//
//            List<String> missing = positiveIds.stream()
//                    .filter(i -> !ids.contains(i)).collect(toList());
//            assertEquals(missing, emptyList());
//
//            List<String> bad = negativeIds.stream().filter(ids::contains).collect(toList());
//            assertEquals(bad, emptyList());
//        }
//    }
}