package br.ufsc.lapesd.riefederator.experiments.federated;

import br.ufsc.lapesd.riefederator.TestUtils;
import br.ufsc.lapesd.riefederator.description.CQueryMatch;
import br.ufsc.lapesd.riefederator.description.Description;
import br.ufsc.lapesd.riefederator.description.molecules.Molecule;
import br.ufsc.lapesd.riefederator.description.molecules.MoleculeMatcher;
import br.ufsc.lapesd.riefederator.experiments.Requests2CSV;
import br.ufsc.lapesd.riefederator.federation.Federation;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.federation.tree.JoinNode;
import br.ufsc.lapesd.riefederator.federation.tree.PlanNode;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Lit;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.reason.tbox.OWLAPITBoxReasoner;
import br.ufsc.lapesd.riefederator.sources.BudgetEndpoints;
import br.ufsc.lapesd.riefederator.webapis.description.APIMoleculeMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.setMinus;
import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.streamPreOrder;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.testng.Assert.*;

public class ProcurementForContractTest extends FederatedQueryExperimentTestBase {
    private static final Lit name = lit("Universidade Federal de Santa Catarina");

    @Override
    protected @Nonnull
    BaseFederatedQueryExperiment createExperiment() {
        return new ProcurementForContract() {
            /** replace start and end dates, to make the query run faster */
            @Override
            protected @Nonnull CQuery createQuery() {
                CQuery query = super.createQuery();
                ArrayList<Triple> triples = new ArrayList<>(query);
                assertEquals(triples.get(0).getPredicate(), p("dataInicioVigencia"));
                assertEquals(triples.get(1).getPredicate(), p("dataFimVigencia"));
                Triple tStart = triples.get(0), tEnd = triples.get(1);
                Triple tStart2 = tStart.withObject(date("2019-12-12"));
                Triple tEnd2   = tEnd.withObject(date("2020-12-05"));
                triples.set(0, tStart2);
                triples.set(1, tEnd2);
                CQuery.WithBuilder builder = CQuery.with(triples).copyAnnotations(query);
                query.getTripleAnnotations(tStart).forEach(a -> builder.annotate(tStart2, a));
                query.getTripleAnnotations(tEnd  ).forEach(a -> builder.annotate(tEnd2,   a));
                return builder.build();
            }
        };
    }

    @DataProvider
    public static @Nonnull Object[][] egMatchData() {
        return Stream.of(
                asList(BudgetEndpoints.contracts().asSource(), CQuery.from(
                        /*  0 */ new Triple(x,  p("dataInicioVigencia"), date("2019-01-01")),
                        /*  1 */ new Triple(x,  p("dataFimVigencia"),   date("2019-12-31")),
                        /*  2 */ new Triple(x,  p("dataAssinatura"),    r3),
                        /*  3 */ new Triple(x,  p("unidadeGestora"), x1),
                        /*  4 */ new Triple(x1, p("orgaoVinculado"), x2),
                        /*  5 */ new Triple(x2, p("codigoSIAFI"),    lit("26246")),
                        /*  6 */ new Triple(x,  p("id"),          b),
                        /*  7 */ new Triple(x , p("dimCompra"), x3),
                        /*  8 */ new Triple(x3, p("numero"),    n),
                        /*  9 */ new Triple(x,  p("unidadeGestora"), x4),
                        /* 10 */ new Triple(x4, p("codigo"),         u)
                )),
                asList(BudgetEndpoints.organizationsByDescription().asSource(), CQuery.from(
                        /* 11 */ new Triple(z,  p("descricao"), name),
                        /* 12 */ new Triple(z,  p("codigo"),    t)
                )),
                asList(BudgetEndpoints.contractById().asSource(), CQuery.from(
                        /* 13 */ new Triple(w,  p("id"),               lit("50235250")),
                        /* 14 */ new Triple(w,  p("modalidadeCompra"), w1),
                        /* 15 */ new Triple(w1, p("descricao"),        d)
                )),
                asList(BudgetEndpoints.procurementModalitiesSource(), CQuery.from(
                        /* 16 */ new Triple(m,  Modalities.hasDescription, d),
                        /* 17 */ new Triple(m,  Modalities.hasCode,        c)
                )),
                asList(BudgetEndpoints.procurementByUnitModalityAndNumber().asSource(), CQuery.from(
                        /* 18 */ new Triple(y,  p("unidadeGestora"), y1),
                        /* 19 */ new Triple(y1, p("codigo"),         lit("153163")),
                        /* 10 */ new Triple(y,  p("licitacao"), y2),
                        /* 21 */ new Triple(y2, p("numero"),    lit("006102018")),
                        /* 22 */ new Triple(y,  p("modalidadeLicitacao"), y3),
                        /* 23 */ new Triple(y3, p("codigo"),              lit("6")),
                        /* 24 */ new Triple(y,  p("id"),                  r1),
                        /* 25 */ new Triple(y,  p("dataResultadoCompra"), r2)
                ))
        ).map(List::toArray).toArray(Object[][]::new);
    }

    @Test(dataProvider = "egMatchData", groups = {"local"})
    public void testMatchEG(@Nonnull Source source, @Nonnull CQuery eg) {
        CQueryMatch match = source.getDescription().match(eg);
        assertFalse(match.isEmpty());
        if (source.getDescription() instanceof APIMoleculeMatcher) {
            assertEquals(match.getKnownExclusiveGroups().size(), 1);
            assertEquals(match.getKnownExclusiveGroups().get(0).getSet(), eg.getSet());
        } else {
            Set<Triple> missing = setMinus(eg, match.getAllRelevant());
            assertEquals(missing, emptySet(), "There are missing triples");
            assertTrue(eg.containsAll(match.getAllRelevant()), "Extraneous triples matched!");
        }
    }

    @DataProvider
    public static @Nonnull Object[][] matchData() {
        return Stream.of(
                asList(BudgetEndpoints.contracts().asSource(), CQuery.from(
                        /*  0 */ new Triple(x,  p("dataInicioVigencia"), date("2019-12-12")),
                        /*  1 */ new Triple(x,  p("dataFimVigencia"),   date("2020-12-05")),
                        /*  2 */ new Triple(x,  p("dataAssinatura"),    r3),
                        /*  3 */ new Triple(x,  p("unidadeGestora"), x1),
                        /*  4 */ new Triple(x1, p("orgaoVinculado"), x2),
                        /*  5 */ new Triple(x2, p("codigoSIAFI"),    t),
                        /*  6 */ new Triple(x,  p("id"),          b),
                        /*  7 */ new Triple(x , p("dimCompra"), x3),
                        /*  8 */ new Triple(x3, p("numero"),    n),
                        /*  9 */ new Triple(x,  p("unidadeGestora"), x4),
                        /* 10 */ new Triple(x4, p("codigo"),         u)
                )),
                asList(BudgetEndpoints.organizationsByDescription().asSource(), CQuery.from(
                        /* 11 */ new Triple(z,  p("descricao"), name),
                        /* 12 */ new Triple(z,  p("codigo"),    t)
                )),
                asList(BudgetEndpoints.contractById().asSource(), CQuery.from(
                        /* 13 */ new Triple(w,  p("id"),               b),
                        /* 14 */ new Triple(w,  p("modalidadeCompra"), w1),
                        /* 15 */ new Triple(w1, p("descricao"),        d)
                )),
                asList(BudgetEndpoints.procurementModalitiesSource(), CQuery.from(
                        /* 16 */ new Triple(m,  Modalities.hasDescription, d),
                        /* 17 */ new Triple(m,  Modalities.hasCode,        c)
                )),
                asList(BudgetEndpoints.procurementByUnitModalityAndNumber().asSource(), CQuery.from(
                        /* 18 */ new Triple(y,  p("unidadeGestora"), y1),
                        /* 19 */ new Triple(y1, p("codigo"),         u),
                        /* 20 */ new Triple(y,  p("licitacao"), y2),
                        /* 21 */ new Triple(y2, p("numero"),    n),
                        /* 22 */ new Triple(y,  p("modalidadeLicitacao"), y3),
                        /* 23 */ new Triple(y3, p("codigo"),              c),
                        /* 24 */ new Triple(y,  p("id"),                  r1),
                        /* 25 */ new Triple(y,  p("dataResultadoCompra"), r2)
                ))
        ).map(List::toArray).toArray(Object[][]::new);
    }

    @Test(dataProvider = "matchData", groups = {"local"})
    public void testNonAPIMatch(@Nonnull Source source, @Nonnull CQuery eg) {
        if (!(source.getDescription() instanceof MoleculeMatcher))
            return; //skip
        Description description = source.getDescription();
        if (description instanceof APIMoleculeMatcher) {
            Molecule molecule = ((APIMoleculeMatcher) source.getDescription()).getMolecule();
            description = new MoleculeMatcher(molecule, OWLAPITBoxReasoner.structural());
        }
        CQueryMatch match = description.match(query);
        assertFalse(match.isEmpty(), "No matches!");
        ImmutableList<CQuery> egs = match.getKnownExclusiveGroups();

        assertEquals(egs.stream().filter(CQuery::isEmpty).collect(toList()),
                     emptySet(), "Empty exclusive groups!");

        List<ImmutableSet<Triple>> matches = egs.stream().map(CQuery::getSet)
                .filter(eg.getSet()::equals).collect(toList());
        assertEquals(matches.size(), 1, "Expected one occurrence of the EG");
    }

    @Test(dataProvider = "matchData", groups = {"local"})
    public void testMatch(@Nonnull Source source, @Nonnull CQuery eg) {
        CQueryMatch match = source.getDescription().match(query);
        if (!(source.getDescription() instanceof APIMoleculeMatcher)
                && !(source.getDescription() instanceof  MoleculeMatcher)) {
            assertFalse(match.isEmpty(),
                    "Expected a match of "+eg.size()+" triples for "+source.getEndpoint());
            assertTrue(match.getKnownExclusiveGroups().isEmpty(),
                    "Expected no EGs for non-molecule source");
            assertTrue(match.getNonExclusiveRelevant().containsAll(eg),
                    "Some triples that should match, did not");
            return; //end here
        }
        // check for EG matches
        if (match.isEmpty() && source.getDescription() instanceof APIMoleculeMatcher) {
            Molecule molecule = ((APIMoleculeMatcher) source.getDescription()).getMolecule();
            MoleculeMatcher m2 = new MoleculeMatcher(molecule, OWLAPITBoxReasoner.structural());
            CQueryMatch match2 = m2.match(query);
            assertFalse(match2.isEmpty(), "Not even plain molecule matches!");
        }

        Set<ImmutableSet<Triple>> matchedSets = match.getKnownExclusiveGroups().stream()
                                                     .map(CQuery::getSet).collect(toSet());
        assertTrue(matchedSets.contains(eg.getSet()), "Expected EG not found");
    }

    protected @Nonnull List<String> relevantSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "ContratoPorId",
                TestUtils.getIdentifyingString(BudgetEndpoints.procurementModalitiesSource()),
                "LicitacaoByUnitModalityAndNumber",
                /* These also match, but should no be used in plans */
                "OrgaoPorCodigo",
                "LicitacaoById"
        );
    }

    protected @Nonnull List<String> uniquelyRelevantSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "LicitacaoByUnitModalityAndNumber",
                /* These also match, but should no be used in plans */
                "OrgaoPorCodigo",
                "LicitacaoById"
        );
    }

    protected @Nonnull List<String> allowedSourceIdentifiers() {
        return asList(
                "Contratos",
                "OrgaoPorDescricao",
                "ContratoPorId",
                TestUtils.getIdentifyingString(BudgetEndpoints.procurementModalitiesSource()),
                "LicitacaoByUnitModalityAndNumber"
        );
    }

    @Override
    protected void checkPlan(@Nonnull PlanNode root) {
        List<PlanNode> joinsWithInputs = streamPreOrder(root).filter(JoinNode.class::isInstance)
                .filter(n -> !n.getInputVars().isEmpty()).collect(toList());
        assertEquals(joinsWithInputs, emptyList(), "This query should yield a plan " +
                                                   "without input variables on join nodes");
    }

    @Test(dependsOnGroups = {"local"})
    public void testExecute() {
        ProcurementForContract exp = new ProcurementForContract();
        Requests2CSV requests2CSV;
        requests2CSV = new Requests2CSV(new File("/tmp/requests.csv"), false);
        try (Federation federation = exp.createFederation(exp.getSources())) {
            requests2CSV.install(federation);
            List<String> ids = new ArrayList<>();
            federation.query(query).forEachRemainingThenClose(
                    s -> ids.add(Objects.requireNonNull(s.get(n)).asLiteral().getLexicalForm()));
            List<String> expected = asList("009692019", "009182019");
            assertEquals(expected.stream().filter(i -> !ids.contains(i)).collect(toList()),
                         emptyList(), "Missing some procurement ids from the result");
        }
    }
}