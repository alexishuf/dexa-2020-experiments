package br.ufsc.lapesd.riefederator.sources;

import br.ufsc.lapesd.riefederator.experiments.VocabContext;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.model.term.Term;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.query.Solution;
import br.ufsc.lapesd.riefederator.webapis.WebAPICQEndpoint;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.testng.Assert.*;

public class RepresentativesEndpointsTest extends VocabContext {
    @DataProvider
    public static Object[][] getRepresentativesData() {
        return Stream.of(
                RepresentativesEndpoints.representativesByState(),
                RepresentativesEndpoints.representativesByParty(),
                RepresentativesEndpoints.representativesByStateAndParty()
        ).map(ep -> new Object[]{ep}).toArray(Object[][]::new);
    }

    @Test(dataProvider = "getRepresentativesData")
    public void testGetRepresentatives(@Nonnull WebAPICQEndpoint ep) {
        List<Term> emails = new ArrayList<>();
        ep.query(CQuery.from(new Triple(x, p("siglaUf"), lit("SC")),
                             new Triple(x, p("siglaPartido"), lit("MDB")),
                             new Triple(x, p("email"), y)))
                .forEachRemainingThenClose(s -> emails.add(s.get(y)));
        // MDB is centrist party very likely to continue having representatives
        // However, they can change names or be wiped out in some future election!
        assertFalse(emails.isEmpty());
        assertTrue(emails.stream().allMatch(Term::isLiteral));
        assertTrue(emails.stream().allMatch(t -> t.asLiteral().getLexicalForm().contains("@")));
    }

    @Test
    public void testGetRepresentativeById() {
        WebAPICQEndpoint ep = RepresentativesEndpoints.representativeById();
        List<Solution> list = new ArrayList<>();
        ep.query(CQuery.with(new Triple(x, p("id"),           tlit(204365)),
                             new Triple(x, p("cpf"),          z),
                             new Triple(x, p("ultimoStatus"), s),
                             new Triple(s, p("email"),        y),
                             new Triple(s, p("gabinete"),     w),
                             new Triple(w, p("telefone"),     t))
                    .project(y, z, t).build()
        ).forEachRemainingThenClose(list::add);

        assertEquals(list.size(), 1);
        Term email = list.get(0).get(y); //privacy concerns
        Term cpf = list.get(0).get(z); //putting CPF in code is dangerous (and likely illegal)
        Term phone = list.get(0).get(t); //Can change yearly or more frequent
        assertNotNull(email);
        assertNotNull(cpf);
        assertNotNull(phone);
        assertTrue(email.asLiteral().getLexicalForm().contains("@"));
        assertTrue(phone.asLiteral().getLexicalForm()
                                    .matches("(\\(\\d+\\))? *\\d+ *- *\\d+"));
    }
}