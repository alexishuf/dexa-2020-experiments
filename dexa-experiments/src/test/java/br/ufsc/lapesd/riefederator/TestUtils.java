package br.ufsc.lapesd.riefederator;

import br.ufsc.lapesd.riefederator.description.molecules.MoleculeMatcher;
import br.ufsc.lapesd.riefederator.federation.Source;
import br.ufsc.lapesd.riefederator.federation.tree.*;
import br.ufsc.lapesd.riefederator.model.Triple;
import br.ufsc.lapesd.riefederator.query.CQuery;
import br.ufsc.lapesd.riefederator.util.IndexedSet;
import br.ufsc.lapesd.riefederator.webapis.description.APIMoleculeMatcher;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Sets;
import org.apache.jena.query.Dataset;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.tdb2.TDB2Factory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static br.ufsc.lapesd.riefederator.federation.planner.impl.JoinInfo.getMultiJoinability;
import static br.ufsc.lapesd.riefederator.federation.planner.impl.JoinInfo.getPlainJoinability;
import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.isAcyclic;
import static br.ufsc.lapesd.riefederator.federation.tree.TreeUtils.streamPreOrder;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toList;
import static org.testng.Assert.*;

public class TestUtils {
    public static void assertPlanAnswers(@Nonnull PlanNode root, @Nonnull CQuery query) {
        assertPlanAnswers(root, query, false);
    }
    public static void assertPlanAnswers(@Nonnull PlanNode root, @Nonnull CQuery query,
                                         boolean allowEmptyNode) {
        IndexedSet<Triple> triples = IndexedSet.from(query.getMatchedTriples());

        // the plan is acyclic
        assertTrue(isAcyclic(root));

        if (!allowEmptyNode) {
            assertFalse(root instanceof EmptyNode, "EmptyNode is not an answer!");
            assertEquals(streamPreOrder(root).filter(EmptyNode.class::isInstance).count(),
                    0, "There are EmptyNodes in the plan as leaves");
        }

        // any query node should only match triples in the query
        List<PlanNode> bad = streamPreOrder(root)
                .filter(n -> n instanceof QueryNode
                        && !triples.containsAll(n.getMatchedTriples()))
                .collect(toList());
        assertEquals(bad, emptyList());

        // any  node should only match triples in the query
        bad = streamPreOrder(root)
                .filter(n -> !triples.containsAll(n.getMatchedTriples()))
                .collect(toList());
        assertEquals(bad, emptyList());

        // the set of matched triples in the plan must be the same as the query
        assertEquals(root.getMatchedTriples(), triples);

        // all nodes in a MQNode must match the exact same triples in query
        // this allows us to consider the MQNode as a unit in the plan
        bad = streamPreOrder(root).filter(n -> n instanceof MultiQueryNode)
                .map(n -> (MultiQueryNode) n)
                .filter(n -> n.getChildren().stream().map(PlanNode::getMatchedTriples)
                        .distinct().count() != 1)
                .collect(toList());
        assertEquals(bad, emptyList());

        // children of MQ nodes may match the same triples with different triples
        // However, if two children have the same triples as query, then their endpoints
        // must not be equivalent as this would be wasteful
        List<Set<QueryNode>> equivSets = streamPreOrder(root)
                .filter(n -> n instanceof MultiQueryNode)
                .map(n -> {
                    Set<QueryNode> equiv = new HashSet<>();
                    ListMultimap<Set<Triple>, QueryNode> mm;
                    mm = MultimapBuilder.hashKeys().arrayListValues().build();
                    for (PlanNode child : n.getChildren()) {
                        if (child instanceof QueryNode)
                            mm.put(((QueryNode) child).getQuery().getSet(), (QueryNode) child);
                    }
                    for (Set<Triple> key : mm.keySet()) {
                        for (int i = 0; i < mm.get(key).size(); i++) {
                            QueryNode outer = mm.get(key).get(i);
                            for (int j = i + 1; j < mm.get(key).size(); j++) {
                                QueryNode inner = mm.get(key).get(j);
                                if (outer.getEndpoint().isAlternative(inner.getEndpoint()) ||
                                        inner.getEndpoint().isAlternative(outer.getEndpoint())) {
                                    equiv.add(outer);
                                    equiv.add(inner);
                                }
                            }
                        }
                    }
                    return equiv;
                }).filter(s -> !s.isEmpty()).collect(toList());
        assertEquals(equivSets, emptySet());

        // no single-child MQ nodes
        bad = streamPreOrder(root)
                .filter(n -> n instanceof MultiQueryNode && n.getChildren().size() < 2)
                .collect(toList());
        assertEquals(bad, emptyList());

        // MQ nodes should not be directly nested (that is not elegant)
        bad = streamPreOrder(root)
                .filter(n -> n instanceof MultiQueryNode
                        && n.getChildren().stream().anyMatch(n2 -> n2 instanceof MultiQueryNode))
                .collect(toList());
        assertEquals(bad, emptyList());

        // all join nodes are valid joins
        bad = streamPreOrder(root).filter(n -> n instanceof JoinNode).map(n -> (JoinNode) n)
                .filter(n -> !getPlainJoinability(n.getLeft(), n.getRight()).isValid())
                .collect(toList());
        assertEquals(bad, emptyList());

        // all join nodes with MQ operands are valid
        bad = streamPreOrder(root).filter(n -> n instanceof JoinNode).map(n -> (JoinNode) n)
                .filter(n -> !getMultiJoinability(n.getLeft(), n.getRight()).isValid())
                .collect(toList());
        assertEquals(bad, emptyList());

        // no single-child cartesian nodes
        bad = streamPreOrder(root)
                .filter(n -> n instanceof CartesianNode && n.getChildren().size() < 2)
                .collect(toList());
        assertEquals(bad, emptyList());

        // cartesian nodes should not be directly nested (that is not elegant)
        bad = streamPreOrder(root)
                .filter(n -> n instanceof CartesianNode
                        && n.getChildren().stream().anyMatch(n2 -> n2 instanceof CartesianNode))
                .collect(toList());
        assertEquals(bad, emptyList());

        // no cartesian nodes where a join is applicable between two of its operands
        bad = streamPreOrder(root).filter(n -> n instanceof CartesianNode)
                .filter(n -> {
                    HashSet<PlanNode> children = new HashSet<>(n.getChildren());
                    //noinspection UnstableApiUsage
                    for (Set<PlanNode> pair : Sets.combinations(children, 2)) {
                        Iterator<PlanNode> it = pair.iterator();
                        PlanNode l = it.next();
                        assert it.hasNext();
                        PlanNode r = it.next();
                        if (getMultiJoinability(l, r).isValid())
                            return true; // found a violation
                    }
                    return false;
                }).collect(toList());
        assertEquals(bad, emptyList());
    }

    public static  @Nonnull String getIdentifyingString(@Nonnull Source source) {
        if (source.getDescription() instanceof MoleculeMatcher)
            return ((APIMoleculeMatcher) source.getDescription()).getMolecule().getCore().getName();
        return source.getName();
    }

    public static @Nonnull Source getByIdentifyingString(@Nonnull Collection<Source> sources,
                                                         @Nonnull String string ) {
        List<Source> list = sources.stream()
                .filter(s -> getIdentifyingString(s).equals(string)).collect(toList());
        assertTrue(list.size() <= 1, "Ambiguous source name "+string);
        assertFalse(list.isEmpty(), "No match for name "+string);
        return list.get(0);
    }

    public static @Nonnull Dataset parseIntoTDB2(@Nonnull Class<?> cls, @Nonnull String resName) {
        Dataset dataset = TDB2Factory.createDataset();
        try (InputStream in = cls.getResourceAsStream(resName)) {
            assertNotNull(in, "resource "+resName+" not found");
            Lang lang = RDFLanguages.filenameToLang(resName);
            assertNotNull(lang, "Could not guess language from "+resName);
            RDFDataMgr.read(dataset, in, lang);
        } catch (IOException e) {
            throw new RuntimeException("IOException while reading resource", e);
        }
        return dataset;
    }
}
