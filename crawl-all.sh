#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
DELAY_MS=${DELAY_MS:-1000}
TARGZ_FILE=${TARGZ_FILE:-}
KEEP=${KEEP:-yes}

get_abs_filename() {
  echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
}

usage() {
  echo -e "Usage: $0 [-d DELAY_MS] [-z TARGZ_FILE] [-k] CRAWL_DIR\n" \
          "\n" \
          " -d DELAY_MS\n" \
          "   Sleep this ammount between the end of a request and the start of \n" \
          "   the next.\n" \
          " -z\n" \
          "   After the crawl is ready, create a compressed acrchive at \n" \
          "   the given file destination. The file path given should include \n" \
          "   the .tar.gz extension.\n" \
          " -K\n" \
          "   Remove the CRAWL_DIR directory after the crawl is complete. This \n" \
          "   has no effect if -z is not given."
}

while getopts "hd:z:K" o; do
  case $o in
  d)
    DELAY_MS=${OPTARG}
    ;;
  z)
    TARGZ_FILE="$(get_abs_filename "${OPTARG}")"
    ;;
  K)
    KEEP=no
    ;;
  h)
    usage
    exit 0
    ;;
  *)
    usage
    exit 1
    ;;
  esac
done
CRAWL_DIR=${@:$OPTIND:1}
NON_EXISTING=${@:$OPTIND+1:1}

if [ -z "$CRAWL_DIR" ]; then
  echo "Positional argument CRAWL_DIR is required!"; usage; exit 1
else
  CRAWL_DIR="$(get_abs_filename "$CRAWL_DIR")"
fi
if [ ! -z "$NON_EXISTING" ]; then
  echo "Too many arguments!"; usage; exit 1
fi

if [ ! -d "$CRAWL_DIR" ]; then
  if ( ! mkdir "$CRAWL_DIR" ); then
    exit 1
  fi
fi

CRAWL_LOG="$CRAWL_DIR/crawl-all.log"
if [ -e "$CRAWL_LOG" ]; then
  mv "$CRAWL_LOG" "${CRAWL_LOG}.old"
fi

work() {
  echo "Crawling into $CRAWL_DIR with delay of $DELAY_MS milliseconds. Will log to $CRAWL_LOG"
  cd "$DIR"

  # Get all SIAFI organizations
  ./run.sh CRAWL --delay-ms $DELAY_MS --out-dir "$CRAWL_DIR" \
    --array-file "${CRAWL_DIR}/siafi.json" \
    'http://www.transparencia.gov.br/api-de-dados/orgaos-siafi{?pagina}'

  # Get all procurements from 2019 for every SIAFI organization
  ./run.sh CRAWL --delay-ms $DELAY_MS --assume-last 10 --out-dir "${CRAWL_DIR}/licitacoes" \
    --start-date 2019-01-01 --end-date 2019-12-31 \
    --array-param codigoOrgao="${CRAWL_DIR}/siafi.json?codigo" \
    'http://www.transparencia.gov.br/api-de-dados/licitacoes{?dataInicial,dataFinal,codigoOrgao,pagina}'

  # Get all contracts
  ./run.sh CRAWL --delay-ms $DELAY_MS --assume-last 10 --out-dir "$CRAWL_DIR/contratos" \
    --start-date 2019-01-01 --end-date 2099-12-31 --no-month-limit \
    --array-param "codigoOrgao=$CRAWL_DIR/siafi.json?codigo" \
    'http://www.transparencia.gov.br/api-de-dados/contratos{?dataInicial,dataFinal,codigoOrgao,pagina}'

  # Get all contracts by id (modalidadeCompra/descricao)
  ./run.sh CRAWL --delay-ms $DELAY_MS --out-dir "$CRAWL_DIR/contratos-id"  \
    --no-paging --naming-param id \
    --obj-param-dir "$CRAWL_DIR/contratos" \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path-filter 'unidadeGestora/orgaoVinculado/codigoSIAFI=26246' \
    --obj-param-path 'id=id' \
    'http://www.transparencia.gov.br/api-de-dados/contratos/id{?id}'

  # Get a mappings from CNPJ to surrogate controactor ids within the portal
  ./run.sh CRAWL --delay-ms $DELAY_MS --out-dir "$CRAWL_DIR/fornecedor-autocomplete" \
    --no-paging \
    --obj-param-dir "$CRAWL_DIR/contratos" \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path-filter 'unidadeGestora/orgaoVinculado/codigoSIAFI=26246' \
    --obj-param-path 'q=fornecedor/codigoFormatado' \
    'http://www.portaldatransparencia.gov.br/criterios/contratos/fornecedor/autocomplete{?q}'

  # Get the procurements of a contractor
  ./run.sh CRAWL --delay-ms $DELAY_MS --out-dir "$CRAWL_DIR/participante-licitacao" \
    --first-page 0 --page-param offset --page-increment 15 \
    --root-path data --anon-name --inject-property idFornecedor=id \
    --obj-param-dir "$CRAWL_DIR/fornecedor-autocomplete" \
    --obj-param-dir-pattern '[0-9]+.json' \
    --obj-param-path 'id=id' \
    'http://www.portaldatransparencia.gov.br/pessoa-juridica/{id}/participante-licitacao/resultado{?id,offset}&paginacaoSimples=true&tamanhoPagina=15&direcaoOrdenacao=desc&colunaOrdenacao=numeroLicitacao&colunasSelecionadas=linkDetalhamento%2Corgao%2CunidadeGestora%2CnumeroLicitacao%2CdataAbertura'


}

work 2>&1 | tee "$CRAWL_LOG"

# Compress
if [ ! -z "$TARGZ_FILE" ]; then
  cd "$(dirname "$CRAWL_DIR")"
  CRAWL_BASE="$(basename "$CRAWL_DIR")"

  echo "Compressing $CRAWL_DIR into $TARGZ_FILE..."
  if [ -e "$TARGZ_FILE" ]; then
    echo "$TARGZ_FILE exists, will overwrite!"
    rm "$TARGZ_FILE" || exit 1
  fi
  sync
  tar zcf "$TARGZ_FILE" "$CRAWL_BASE" || exit 1
  du -h "$TARGZ_FILE"

  # Remove?
  if [ "$KEEP" = "no" ]; then
    echo "Will remove crawled data directory $CRAWL_DIR, since it was correctly" \
         "compressed into $TARGZ_FILE"
    rm -fr "$CRAWL_DIR"
  fi
  cd "$DIR"
fi

